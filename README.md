<div align="center"><img  alt="🏦"  aria-label="🏦"  src="https://notion-emojis.s3-us-west-2.amazonaws.com/prod/svg-twitter/1f3e6.svg"  width="250px"> <br></br></div>
<div align="center"><h1>Projeto LisoTracker™<br>
Finanças Pessoais Web API</h1></div>

> Equipe:<br>
>Antonio Alessandro Rocha Beserra<br>
>Francisco Gabriel de Oliveira<br>
>Fábio Ricardo de Lima Souza<br>
>José Victor da Silva Rocha<br>
>Wana Batista Barbosa<br>


## 1. Introdução

<div  align="justify"style="font-size: 1em;">A gestão financeira é um processo de organização, planejamento e controle das atividades financeiras, históricamente tratada como algo relevante apenas ao ramo empresárial e pessoas com grandes ganhos pecuniários,
deixou por muito tempo a maioria das pessoas desatentas em relação à gestão dos seus recursos pessoais. <br><br>Atualmente a aplicação desses conceitos também integram as finanças pessoais independentemente do volume dos ganhos. Essa mudança de mentalidade aumentou a busca por meios que possam auxiliar a gestão das atividades financeiras individuais a fim de permitir um controle maior sobre tudo que entra e sai da sua carteira (ou na conta bancária).<br><br>O projeto Liso Tracker "Finanças Pessoais Web API" tem como objetivo principal ser uma ferramenta eficiente e de fácil usabilidade para os usuários realizarem o controle de suas finanças pessoais. A aplicação permite registrar despesas e receitas, categorizar transações, acompanhar saldos e gerar relatórios financeiros de forma intuitiva e simplificada. O produto deste projeto é formado pela integração da interface de programação de aplicativos (API) e interface gráfica.</div>

## 2. Propósito

<div align="justify"style="font-size: 1em;"> A Web API de Liso Tracker foi desenvolvida para fornecer uma interface robusta e segura para gerenciar finanças pessoais. Por meio  dessa API, os usuários podem realizar operações financeiras ssenciais, como gestão de contas, registro e acompanhamento de transações, geração de relatórios e análises financeiras.</div>

## 3. Principais Funcionalidades
<div align="justify"style="font-size: 1em;">

<b>Gestão de Contas</b><br>
Consulta e Atualização: Oferece endpoints para consultar os detalhes das contas disponíveis (Carteira, Poupança e Conta Corrente) e atualizar informações conforme necessário.
 
<b>Transações Financeiras</b><br>
Registro de Transações: Facilita o registro de diferentes tipos de transações 
financeiras, como despesas e receitas;
<br>

Consulta e Histórico: Permite obter o histórico de transações, 
tipo e categoria; e<br>

Atualização, Exclusão: Oferece funcionalidades para atualizar ou excluir transações registradas.
 
<b>Relatórios e Análises</b><br>
Geração de Relatórios: Possibilita a criação de relatórios financeiros (demonstrativos de resultados, e fluxo de caixa); e <br>

Análise Financeira: Fornece ferramentas para análises financeiras,
permitindo identificar padrões de despesas e receitas (maiores despesas e maiores receitas).

<b>Autenticação e Autorização</b><br>
Autenticação Segura: Utiliza tokens JWT para garantir que apenas usuários autenticados possam acessar a API; e <br>
Controle de Acesso: Implementa níveis de permissão para diferentes operações, garantindo que apenas usuários autorizados possam realizar determinadas ações. </div>

## 4. Requisitos do sistema 
•	Cadastro de despesas e de capital, categorização, autenticação de usuários e relatórios.

#### 4.1 Requisitos funcionais:

<div align="justify"style="font-size: 1em;">

 
<b>Transações Financeiras</b><br>
Registro de Transações: A API permite o registro de novas transações financeiras (despesas e receitas).

Consulta de Transações: A API permite a consulta de transações por categoria.

Atualização de Transações: A API permite a atualização das transações existentes.

<b>Relatórios e Análises</b><br>
Geração de Relatórios: A API permite a geração de relatórios financeiros (demonstrativos de resultados e fluxo de caixa); e

Segregação por tipo: A API permite a categorização dos tipos de despesas e receitas.


<b>Autenticação e Autorização</b><br>
Registro e Login de Usuários: A API permite o registro de novos usuários e o login de usuários existentes.

Autenticação Segura: A API implementa mecanismos de autenticação (JWT).

Controle de Acesso: A API gerencia permissões de usuários para diferentes operações (Admin e usuário padrão). </div>

#### 4.2 Requisitos Não Funcionais:
 
<b>Segurança</b> <br>
Proteção de Dados: A API garante encriptação da senha do usuário; e<br>

Autorização: A API garante que apenas usuários autorizados possam acessar recursos específicos.
 
<b>Logs de Erro</b><br>
 A API fornece logs detalhados para facilitar a depuração e manutenção.
  
<b>Usabilidade</b><br>
Documentação Clara: A API tem uma documentação clara e acessível para desenvolvedores; e <br>
 
<b>Compatibilidade</b> <br>
Suporte a Vários Dispositivos: A API é acessível a partir de diferentes dispositivos (web); e <br>
Interoperabilidade: A API é compatível com diferentes sistemas operacionais e plataformas de desenvolvimento.
 
<b>Eficiência</b><br>
Uso de Recursos: A API otimiza o uso de recursos do servidor para minimizar custos e maximizar a eficiência.
 
<b>Auditabilidade</b><br>
Registro de Atividades: A API mantém um registro detalhado de todas as atividades e transações para auditoria e rastreamento.

## 5. Estrutura do Projeto
- ASP.NET Core para a criação da  API RESTful
- Blazor para a interface do usuario
- Entity Framework usando Sql server como BD
- JWT para a autenticação
- Dotnet 8.0

### 5.1 Sugestão para rodar o projeto localmente
- IDE Visual Studio 2022
- Criar uma base de dados local (sql server management studio) <br> 
- Abra o console do Nuget:
>Ferramentas -> Nuget -> console <br>
 - Executar os comandos do Migration:<br>
 >Selecione o projeto padrão (FinançasPessoaisWebAPI.Infra.Data)
~~~C#
 ADD-MIGRATION INITIAL
 UPDATE-DATABASE
~~~

### 5.2 Diagrama Entidades e Relacionamentos

<div align="center"><p>Figura 1. Diagrama E-R </p><img src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/Diagrama_E-R.png?ref_type=heads"  width="1000px"> <br></br></div>

## 6. Resultado
<div align="justify"style="font-size: 1em;">
 O resultado da aplicação excedeu nossas expectativas. Com uma interface intuitiva e funcionalidades robustas, estamos confiantes de que atenderá as necessidades dos usuários de forma eficiente. A equipe demonstrou eficácia no desenvolvimento do projeto, com atenção ao cumprimento dos prazos e a qualidade do produto. Abaixo apresentamos figuras da API e Interface gráfica: </div>


<div align="center"><p>Figura 3. Web API</p><img src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/tmp_7e844066-b7a3-49ee-8d71-2ad50ffe8711.png">

<img src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/tmp_d1b7fcb6-d627-4023-8c62-dbedeac53610.png"> <br></br></div>


<div align="center"><p>Figura 4. Interface gráfica: Login </p><img src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/LisoTracker_-_Login_Page.png?ref_type=heads"  width="1000px"> <br></br></div>


<div align="center"><p>Figura 5. Interface gráfica: Home page</p><img  src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/LisoTracker_-_Home_Page.png?ref_type=heads"  width="1000px" > <br></br></div>

<div align="center"><p>Figura Interface gráfica: Cadastro de Transações</p><img  src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/LisoTracker_-_Transactions_-__New_Income_.png?ref_type=heads"  width="1000px"> <br></br></div>

<div align="center"><p>Figura 6. Interface gráfica: Histórico de transações</p><img  src="https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi/-/raw/master/Imagens/LisoTracker_-_Transactions__All_.png?ref_type=heads"  width="1000px"> <br></br></div>

 ### 7. Entregáveis
- [x] API RESTful
- [x] FrontEnd consumindo a API
- [x] Choro e Lágrimas do Gabriel


                                            Projeto LisoTracker © 2024