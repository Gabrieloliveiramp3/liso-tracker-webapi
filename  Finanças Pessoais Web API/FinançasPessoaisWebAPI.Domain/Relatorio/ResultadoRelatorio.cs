﻿using static FinançasPessoaisWebAPI.Domain.Entities.Conta;

namespace FinançasPessoaisWebAPI.Domain.Relatorio;

public class ResultadoRelatorio
{
    public decimal Total_Receita { get; set; }
    public decimal Total_Despesa { get; set; }
    public decimal SaldoAtual { get; set; }
    public Dictionary<TipoConta, decimal> TotalPorConta { get; set; }

    public Dictionary<string, decimal> TotalPorMes { get; set; }
}