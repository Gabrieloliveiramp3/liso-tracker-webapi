﻿using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Domain.Account;

public interface IAuthenticate
{
    Task<bool> AuthenticateAsync(string email, string senha);

    Task<bool> UserExistAsync(string email);

    public string GenerateToken(int id, string email);

    public Task<Usuario> GetUserByEmail(string email);
}