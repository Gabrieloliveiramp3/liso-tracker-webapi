﻿namespace FinançasPessoaisWebAPI.Domain.Paginacao;

public class Paginas<T> : List<T>
{
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }
    public int Quantidade { get; set; }
    public int TotalCount { get; set; }

    public Paginas(IEnumerable<T> items, int pageNumber, int pageSize, int count)
    {
        CurrentPage = pageNumber;
        TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        Quantidade = pageSize;
        TotalCount = count;

        AddRange(items);
    }

    public Paginas(IEnumerable<T> items, int currentPage, int totalPages, int pageSize, int totalCount)
    {
        CurrentPage = currentPage;
        TotalPages = totalPages;
        Quantidade = pageSize;
        TotalCount = totalCount;

        AddRange(items);
    }
}