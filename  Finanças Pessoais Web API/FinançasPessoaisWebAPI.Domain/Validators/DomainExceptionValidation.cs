﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FluentValidation;
using static FinançasPessoaisWebAPI.Domain.Entities.Categoria;

namespace FinançasPessoaisWebAPI.Domain.Validators;

public class TransacaoValidator : AbstractValidator<Transacao>
{
    public TransacaoValidator()
    {
        RuleFor(t => t.Valor)
            .NotEmpty().WithErrorCode("O valor da transação é obrigatório.").GreaterThan(1).LessThan(10).WithErrorCode("O valor da transação deve ser um valor positivo.").ScalePrecision(2, 18).WithErrorCode("O valor deve ter no máximo 18 dígitos e 2 casas decimais.");

        RuleFor(t => t.Descricao)
            .MaximumLength(50)
            .WithErrorCode(" Descrição pode ter no máximo 50 caracteres.");

        RuleFor(t => t.Data)
            .NotEmpty()
            .WithErrorCode("A data da transação é obrigatória.");

        RuleFor(t => t.UsuarioId)
            .NotEmpty()
            .WithErrorCode("O ID do usuário é obrigatório.");
    }
}

public class CategoriaValidator : AbstractValidator<Categoria>
{
    public CategoriaValidator()
    {
        RuleFor(c => c.tipoTransacao)
            .IsInEnum().WithErrorCode("O campo tipoTransacao é obrigatório e deve ser um valor válido do enum TipoTransacao.");

        RuleFor(c => c.categoriaReceita)
            .IsInEnum().When(c => c.tipoTransacao == TipoTransacao.Receita)
            .WithErrorCode("O campo categoriaReceita deve ser um valor válido do enum CategoriaReceita quando o tipoTransacao é Receita.");

        RuleFor(c => c.categoriaDespesa)
            .IsInEnum().When(c => c.tipoTransacao == TipoTransacao.Despesa)
            .WithErrorCode("O campo categoriaDespesa deve ser um valor válido do enum CategoriaDespesa quando o tipoTransacao é Despesa.");
    }
}

public class UsuarioValidator : AbstractValidator<Usuario>
{
    public UsuarioValidator()
    {
        RuleFor(e => e.Transacaos).NotEmpty()
            .WithErrorCode("O Valor não pode ser nula");

        RuleFor(e => e.Email).EmailAddress();

        RuleFor(e => e.SenhaNormal).NotEmpty()
             .WithErrorCode("A Senha não pode ser vazia");

        RuleFor(e => e.SenhaNormal).NotNull()
            .WithErrorCode("A Senha não pode ser nula");
    }
}

public class ContaValidator : AbstractValidator<Conta>
{
    public ContaValidator()
    {
        RuleFor(c => c.Id)
                    .NotEmpty()
                    .WithErrorCode("O campo Id é obrigatório.");

        RuleFor(c => c.tipoConta)
                    .IsInEnum()
                    .WithErrorCode("O campo tipoConta é obrigatório e deve ser um valor válido do enum TipoConta.");
    }
}