﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Usuario
{
    [Key]
    public int Id { get; set; }

    public string Nome { get; set; } = string.Empty;

    public string Email { get; set; }

    public bool Admin { get; set; } = false;
    public byte[] SenhaNormal { get; set; }
    public byte[] SenhaHash { get; set; }
    public ICollection<Transacao> Transacaos { get; } = new List<Transacao>();

    //Um usuario pode fazer várias transações

    public void AlterarSenha(byte[] senhaNormal, byte[] senhaHash)
    {
        SenhaNormal = senhaNormal;
        SenhaHash = senhaHash;
    }

    public void SetAdmin(bool admin)
    {
        Admin = admin;
    }

    public Usuario()
    {
        //Hashset não permite duplicatas, com isso garantimos que não teremos Ids iguais quando for criado.
        Transacaos = new HashSet<Transacao>();
    }
}