﻿namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Transacao
{
    public int Id { get; set; }
    public int UsuarioId { get; set; }
    public decimal Valor { get; set; }
    public string Descricao { get; set; }

    public DateTime Data { get; set; } = DateTime.Now;

    // Propriedades de navegação
    public virtual Categoria Categoria { get; set; }

    public virtual Usuario Usuario { get; set; }
    public virtual Conta Conta { get; set; }
}

// Relacionamento Um para Muitos (1:N) : Uma transação deve ser registrada em apenas uma conta, mas uma conta pode ter diversas transações relacionadas.
//Relacionamento Um para Muitos(1:N): Uma transação deve ser registrada em apenas uma conta, mas uma conta pode ter diversas transações relacionadas.