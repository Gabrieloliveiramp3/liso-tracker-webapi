﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Paginacao;

namespace FinançasPessoaisWebAPI.Infra.Data.Interfaces;

public interface ITransacaoRepository
{
    Task<Paginas<Transacao>> GetAllTransacaoReceitaAsync(int userId, int pageNumber, int pageSize);

    Task<Paginas<Transacao>> GetAllTransacaoDespesaAsync(int userId, int pageNumber, int pageSize);

    Task<Transacao> GetTransacoesByUsuarioIdAsync(int userId, int Id);

    Task<Transacao> EditTransacaoDespesaAsync(int userId, Transacao transacao);

    Task<Transacao> EditTransacaoReceitaAsync(int userId, Transacao transacao);

    Task<Transacao> RemoveTransacao(int userId, int id);

    Task<Transacao> CreateTransacaoReceitasAsync(int userId, Transacao transacao);

    Task<Transacao> CreateTransacaoDespesasAsync(int userId, Transacao transacao);
}