﻿using FinançasPessoaisWebAPI.Domain.Relatorio;

namespace FinançasPessoaisWebAPI.Infra.Data.Interfaces;

public interface IRelatorioRepository
{
    ResultadoRelatorio Relatorios(int userId);
}