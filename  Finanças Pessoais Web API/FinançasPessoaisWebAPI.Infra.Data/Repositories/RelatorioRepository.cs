﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Relatorio;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using System.Globalization;
using static FinançasPessoaisWebAPI.Domain.Entities.Categoria;
using static FinançasPessoaisWebAPI.Domain.Entities.Conta;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

/// <summary>
/// Repositório para gerar relatórios de transações de receitas e despesas.
/// </summary>
public class RelatorioRepository : IRelatorioRepository
{
    private readonly FinançasPessoaisWebApiApiContext _relatorio;

    public RelatorioRepository(FinançasPessoaisWebApiApiContext relatorio)
    {
        _relatorio = relatorio;
    }

    public ResultadoRelatorio Relatorios(int userId)
    {
        var relatorio = (from p in _relatorio.Transacaos
                         where p.UsuarioId == userId
                         group p by new { p.Categoria.tipoTransacao, p.Conta.tipoConta } into g
                         select new Relatorio()
                         {
                             TipoTransacao = g.Key.tipoTransacao,
                             TipoConta = g.Key.tipoConta,
                             Soma = g.Sum(p => p.Valor)
                         }).ToList();

        var totalReceita = relatorio.Where(r => r.TipoTransacao == TipoTransacao.Receita).Sum(r => r.Soma);
        var totalDespesa = relatorio.Where(r => r.TipoTransacao == TipoTransacao.Despesa).Sum(r => r.Soma);
        var saldoAtual = totalReceita - totalDespesa;

        var totalPorConta = new Dictionary<TipoConta, decimal>();
        foreach (var item in relatorio)
        {
            if (!totalPorConta.ContainsKey(item.TipoConta))
            {
                totalPorConta[item.TipoConta] = 0;
            }

            if (item.TipoTransacao == TipoTransacao.Despesa)
            {
                totalPorConta[item.TipoConta] -= item.Soma;
            }
            else
            {
                totalPorConta[item.TipoConta] += item.Soma;
            }
        }

        static string ObterNomeMes(int numeroMes)
        {
            var cultura = CultureInfo.CreateSpecificCulture("pt-BR");
            return cultura.DateTimeFormat.GetMonthName(numeroMes);
        }

        var transacoesPorMes = _relatorio.Transacaos
    .Where(t => t.UsuarioId == userId)
    .GroupBy(t => new { t.Data.Month, t.Data.Year })
    .Select(g => new
    {
        Mes = g.Key.Month,
        Ano = g.Key.Year,
        Total = g.Sum(t => t.Categoria.tipoTransacao == TipoTransacao.Despesa ? -t.Valor : t.Valor)
    })
    .OrderBy(x => x.Ano)// Ordena por ano crescente
    .ThenBy(x => x.Mes) // Em caso de empate no ano, ordena por mês
    .ToDictionary(x => $"{ObterNomeMes(x.Mes)}/{x.Ano}", x => x.Total);

        return new ResultadoRelatorio
        {
            Total_Receita = totalReceita,
            Total_Despesa = totalDespesa,
            SaldoAtual = saldoAtual,
            TotalPorConta = totalPorConta,
            TotalPorMes = transacoesPorMes
        };
    }
}