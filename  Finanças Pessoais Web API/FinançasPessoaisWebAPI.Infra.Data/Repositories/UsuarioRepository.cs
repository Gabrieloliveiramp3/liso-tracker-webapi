using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

public class UsuarioRepository : IUsuarioRepository
{
    private readonly FinançasPessoaisWebApiApiContext _usuario;

    public UsuarioRepository(FinançasPessoaisWebApiApiContext usuario)
    {
        _usuario = usuario;
    }

    public async Task<IEnumerable<Usuario>> GetAllUsuarioAsync()
    {
        try
        {
            return await _usuario.Usuarios.ToListAsync();
        }
        catch (Exception ex)
        {
            throw new Exception("Ocorreu um erro ao recuperar todos os usuários.", ex);
        }
    }

    public async Task<Usuario> GetUsuarioByIdAsync(int id)
    {
        try
        {
            return await _usuario.Usuarios.FindAsync(id);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao recuperar o usuário com ID {id}.", ex);
        }
    }

    public async Task<Usuario> CreateUsuarioAsync(Usuario usuario)
    {
        if (usuario == null)
        {
            throw new ArgumentNullException(nameof(usuario), "O objeto de usuário fornecido não pode ser nulo");
        }

        try
        {
            await _usuario.Usuarios.AddAsync(usuario);
            await _usuario.SaveChangesAsync();
            return usuario;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao salvar o usuário.", ex);
        }
        catch (Exception ex)
        {
            throw new Exception("Ocorreu um erro ao criar o usuário.", ex);
        }
    }

    /// <summary>
    /// Edita um Usuário
    /// </summary>
    /// <param name="usuario"></param>
    /// <returns></returns>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="Exception"></exception>
    public async Task<Usuario> EditUsuarioAsync(Usuario usuario)
    {
        if (usuario == null)
        {
            throw new ArgumentNullException(nameof(usuario), "O objeto de usuário fornecido não pode ser nulo");
        }

        try
        {
            var usuarioConsultado = await _usuario.Usuarios.FindAsync(usuario.Id);
            if (usuarioConsultado == null)
            {
                return null;
            }

            usuarioConsultado.Admin = usuario.Admin;
            usuarioConsultado.SenhaNormal = usuario.SenhaNormal;
            usuarioConsultado.SenhaHash = usuario.SenhaHash;
            usuarioConsultado.Nome = usuario.Nome;
            usuarioConsultado.Id = usuario.Id;

            _usuario.Usuarios.Update(usuarioConsultado);
            await _usuario.SaveChangesAsync();
            return usuarioConsultado;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao atualizar o usuário com ID {usuario.Id}.", ex);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao atualizar o usuário.", ex);
        }
    }

    public async Task<Usuario> RemoveUsuario(int id)
    {
        try
        {
            var usuario = await _usuario.Usuarios.FindAsync(id);
            if (usuario != null)
            {
                _usuario.Usuarios.Remove(usuario);
                await _usuario.SaveChangesAsync();
            }
            return usuario;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao remover o usuário com ID {id}.", ex);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao remover o usuário.", ex);
        }
    }

    public async Task<bool> UserExiste()
    {
        try
        {
            return await _usuario.Usuarios.AnyAsync();
        }
        catch (Exception ex)
        {
            throw new Exception("Ocorreu um erro ao verificar usuários existentes.", ex);
        }
    }
}