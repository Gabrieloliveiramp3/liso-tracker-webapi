﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration;

public class TransacaoConfiguration : IEntityTypeConfiguration<Transacao>
{
    public void Configure(EntityTypeBuilder<Transacao> builder)
    {
        builder.HasKey(t => t.Id);

        // Configuração para a propriedade Data (valor padrão é a data atual)
        builder.Property(t => t.Data).HasDefaultValueSql("GETDATE()");

        builder.Property(t => t.UsuarioId)
            .IsRequired();

        builder.Property(t => t.Valor)
           .IsRequired()
   .HasColumnType("decimal(18, 2)")
   .HasPrecision(18, 2)
   .HasConversion<double>()
   .HasAnnotation("MinLength", 1); // Garante que o valor seja maior que 0

        builder.Property(t => t.Descricao)
            .HasMaxLength(255);

        builder.Property(t => t.Data)
            .IsRequired();
    }
}