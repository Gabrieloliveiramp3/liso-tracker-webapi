﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration;

public class UsuarioConfiguration : IEntityTypeConfiguration<Usuario>
{
    public void Configure(EntityTypeBuilder<Usuario> builder)
    {
        builder.Property(u => u.Nome)
          .HasMaxLength(255);

        builder.Property(u => u.Email).IsRequired()
          .HasMaxLength(255);
        builder.Property(u => u.Admin);

        builder.Property(u => u.SenhaNormal);

        builder.Property(u => u.SenhaHash);
    }
}