﻿using FinançasPessoaisWebAPI.Domain.Paginacao;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Helper;

public class PaginasHelper
{
    // Método para criar uma página paginada de itens
    public static async Task<Paginas<T>> CreateAsync<T>
            (IQueryable<T> source, int pageNumber, int pageSize) where T : class
    {
        // Conta o total de itens na fonte de dados
        var count = await source.CountAsync();

        // Obtém os itens da página atual com base no número da página e tamanho da página
        var items = await source.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToListAsync();

        // Cria e retorna um objeto Paginas<T> com os itens, informações de paginação e contagem total
        return new Paginas<T>(items, pageNumber, pageSize, count);
    }
}