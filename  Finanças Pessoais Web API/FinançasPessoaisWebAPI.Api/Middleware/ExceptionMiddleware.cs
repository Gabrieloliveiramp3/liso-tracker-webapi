﻿using FinançasPessoaisWebAPI.Api.Errors;
using System.Net;
using System.Text.Json;

namespace FinançasPessoaisWebAPI.Api.Middleware;

public class ExceptionMiddleware
{
    private readonly RequestDelegate _next;
    private readonly ILogger<ExceptionMiddleware> _logger;
    private readonly IHostEnvironment _env;

    public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger, IHostEnvironment env)
    {
        _next = next;
        _logger = logger;
        _env = env;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            // Executa o próximo middleware na cadeia de processamento
            await _next(context);
        }
        catch (Exception ex)
        {
            // Registra o erro no log
            _logger.LogError(ex, ex.Message);
            context.Response.ContentType = "application/json";

            // Define o status do código de resposta com base no tipo de exceção
            if (ex is UnauthorizedAccessException)
            {
                // Se a exceção for de acesso não autorizado, retorna um erro 401 (Unauthorized)
                context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                var response = new ApiException(
                    context.Response.StatusCode.ToString(),
                    "Usuário não autorizado, você precisa se autenticar",
                    ex.StackTrace);
                var json = JsonSerializer.Serialize(response, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
                await context.Response.WriteAsync(json);
            }
            else
            {
                // Caso contrário, retorna um erro 500 (Internal Server Error)
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                var response = _env.IsDevelopment() ?
                    new ApiException(
                        context.Response.StatusCode.ToString(),
                        ex.Message,
                        ex.StackTrace) :
                    new ApiException(
                        context.Response.StatusCode.ToString(),
                        "Internal server error",
                        "Detalhes do erro não disponíveis em produção.");
                var json = JsonSerializer.Serialize(response, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
                await context.Response.WriteAsync(json);
            }
        }
    }
}