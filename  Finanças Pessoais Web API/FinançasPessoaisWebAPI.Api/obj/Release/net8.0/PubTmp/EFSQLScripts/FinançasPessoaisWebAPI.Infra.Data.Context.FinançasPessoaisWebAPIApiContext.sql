﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE TABLE [Categorias] (
        [Id] int NOT NULL IDENTITY,
        [categoria] nvarchar(50) NULL,
        [Tipo] bit NOT NULL,
        CONSTRAINT [PK_Categorias] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE TABLE [Usuarios] (
        [Id] int NOT NULL IDENTITY,
        [NomeUsuario] nvarchar(250) NOT NULL,
        [Email] nvarchar(200) NOT NULL,
        [Admin] bit NOT NULL,
        [SenhaNormal] varbinary(max) NOT NULL,
        [SenhaHash] varbinary(max) NOT NULL,
        CONSTRAINT [PK_Usuarios] PRIMARY KEY ([Id])
    );
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE TABLE [Contas] (
        [Id] int NOT NULL IDENTITY,
        [Agencia] int NOT NULL,
        [Numero] int NOT NULL,
        [Banco] nvarchar(10) NOT NULL,
        [UsuarioId] int NOT NULL,
        CONSTRAINT [PK_Contas] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Contas_Usuarios_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [Usuarios] ([Id]) ON DELETE CASCADE
    );
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE TABLE [Transacaos] (
        [Id] int NOT NULL IDENTITY,
        [Valor] decimal(18,2) NOT NULL,
        [Descricao] nvarchar(50) NULL,
        [Data] datetime2 NOT NULL,
        [CategoriaId] int NOT NULL,
        [UsuarioId] int NOT NULL,
        [ContaId] int NOT NULL,
        CONSTRAINT [PK_Transacaos] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_Transacaos_Categorias_CategoriaId] FOREIGN KEY ([CategoriaId]) REFERENCES [Categorias] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Transacaos_Contas_ContaId] FOREIGN KEY ([ContaId]) REFERENCES [Contas] ([Id]) ON DELETE CASCADE,
        CONSTRAINT [FK_Transacaos_Usuarios_UsuarioId] FOREIGN KEY ([UsuarioId]) REFERENCES [Usuarios] ([Id])
    );
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Tipo', N'categoria') AND [object_id] = OBJECT_ID(N'[Categorias]'))
        SET IDENTITY_INSERT [Categorias] ON;
    EXEC(N'INSERT INTO [Categorias] ([Id], [Tipo], [categoria])
    VALUES (1, CAST(1 AS bit), N''Salário''),
    (2, CAST(1 AS bit), N''Benefícios''),
    (3, CAST(1 AS bit), N''Vendas''),
    (4, CAST(1 AS bit), N''Economias''),
    (5, CAST(1 AS bit), N''Outros''),
    (6, CAST(0 AS bit), N''Saúde''),
    (7, CAST(0 AS bit), N''Aluguel''),
    (8, CAST(0 AS bit), N''Educação''),
    (9, CAST(0 AS bit), N''Lazer''),
    (10, CAST(0 AS bit), N''Alimentação''),
    (11, CAST(0 AS bit), N''Despesas_pessoais''),
    (12, CAST(0 AS bit), N''Outros'')');
    IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'Tipo', N'categoria') AND [object_id] = OBJECT_ID(N'[Categorias]'))
        SET IDENTITY_INSERT [Categorias] OFF;
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE UNIQUE INDEX [IX_Contas_UsuarioId] ON [Contas] ([UsuarioId]);
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE INDEX [IX_Transacaos_CategoriaId] ON [Transacaos] ([CategoriaId]);
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE INDEX [IX_Transacaos_ContaId] ON [Transacaos] ([ContaId]);
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    CREATE INDEX [IX_Transacaos_UsuarioId] ON [Transacaos] ([UsuarioId]);
END;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508114609_wana_scrum_master_mais_fraca_do_BNB'
)
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20240508114609_wana_scrum_master_mais_fraca_do_BNB', N'8.0.4');
END;
GO

COMMIT;
GO

BEGIN TRANSACTION;
GO

IF NOT EXISTS (
    SELECT * FROM [__EFMigrationsHistory]
    WHERE [MigrationId] = N'20240508162933_AddProperty'
)
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20240508162933_AddProperty', N'8.0.4');
END;
GO

COMMIT;
GO

