﻿using FinançasPessoaisWebAPI.Api.Models;
using FinançasPessoaisWebAPI.Application.DTOs.Usuario;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Account;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[Route("[controller]")]
[ApiController]
public class AutenticaçãoController : ControllerBase
{
    private readonly IAuthenticate _authenticateService;
    private readonly IUsuarioService _usuarioService;

    public AutenticaçãoController(IUsuarioService usuarioService,
                                  IAuthenticate authenticateService)
    {
        _usuarioService = usuarioService;
        _authenticateService = authenticateService;
    }

    /// <summary>
    /// Registra um novo usuário.
    /// </summary>
    /// <param name="usuarioRegisterDto">Dados de registro do novo usuário.</param>
    /// <returns>
    /// Retorna `BadRequest` se os dados do usuário forem nulos ou se o e-mail já estiver cadastrado.
    /// Retorna `UserToken` com um token gerado se o usuário for criado com sucesso.
    /// </returns>
    [HttpPost]
    [Route("/Registrar")]
    public async Task<ActionResult<UserToken>> CreateUsuarioAsync(UsuarioRegisterDto usuarioRegisterDto)
    {
        if (usuarioRegisterDto == null) return BadRequest("Ocorreu um erro ao criar um usuário");

        var emailExiste = await _authenticateService.UserExistAsync(usuarioRegisterDto.Email);

        if (emailExiste) return BadRequest("E-mail já possui um cadastro.");

        var existeusuario = await _usuarioService.UserExiste();

        if (!existeusuario)
        {
            usuarioRegisterDto.Admin = true;
        }

        var usuario = await _usuarioService.CreateUsuarioAsync(usuarioRegisterDto);
        if (usuario == null) return BadRequest("Erro ao cadastrar");

        var token = _authenticateService.GenerateToken(usuario.Id, usuario.Email);

        return new UserToken
        {
            Mensagem = "Cadastro realizado com sucesso",
            Token = token,
            Admin = usuario.Admin
        };
    }

    /// <summary>
    ///     Autentica um usuário.
    /// </summary>
    /// <param name="login">Dados de login do usuário.</param>
    /// <returns>
    ///     Retorna `Unauthorized` se o usuário não existir ou se a senha for inválida.
    ///     Retorna `UserToken` com um token gerado se a autenticação for bem-sucedida.
    /// </returns>
    [HttpPost]
    [Route("/Login")]
    public async Task<ActionResult<UserToken>> Selecionar(Login login)
    {
        var existe = await _authenticateService.UserExistAsync(login.Email);
        if (!existe) return Unauthorized("Usuário non ecziste.");

        var result = await _authenticateService.AuthenticateAsync(login.Email, login.Senha);

        if (!result) return Unauthorized("Usuário ou senha inválida.");

        var usuario = await _authenticateService.GetUserByEmail(login.Email);
        var token = _authenticateService.GenerateToken(usuario.Id, usuario.Email);

        return new UserToken
        {
            Mensagem = $"Bem-Vindo(a) {usuario.Nome}",
            Token = token,
            Admin = usuario.Admin
        };
    }
}