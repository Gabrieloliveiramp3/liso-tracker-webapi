﻿using FinançasPessoaisWebAPI.Application.DTOs.Usuario;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Infra.Ioc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize]
public class UsuarioController : Controller
{
    private readonly IUsuarioService _usuarioService;
    private readonly IMemoryCache _cache;

    public UsuarioController(IUsuarioService usuarioService, IMemoryCache cache)
    {
        _usuarioService = usuarioService;
        _cache = cache;
    }

    /// <summary>
    /// Obtém um usuário pelo ID.
    /// </summary>
    /// <param name="id">O ID do usuário a ser consultado</param>
    /// <returns>Resultado contendo o usuário ou uma mensagem de erro.</returns>
    [HttpGet]
    [Route("{id}")]
    public async Task<IActionResult> GetUsuarioByIdAsync(int id)
    {
        var userId = User.GetId();
        var usuario = await _usuarioService.GetUsuarioByIdAsync(userId);
        if (!usuario.Admin) return Unauthorized("Somente os Semi-Deuses podem consultar User");

        // Verifica se o usuário está em cache
        if (!_cache.TryGetValue($"Usuario_{id}", out UsuarioDto usuarioDtoGetId))
        {
            // Não está em cache, busca no serviço
            usuarioDtoGetId = await _usuarioService.GetUsuarioByIdAsync(id);

            // Armazena em cache por 10 minutos
            _cache.Set($"Usuario_{id}", usuarioDtoGetId, TimeSpan.FromMinutes(30));
        }

        if (usuarioDtoGetId == null) return NotFound("Usuário não encontrado");

        return Ok(usuarioDtoGetId);
    }

    /// <summary>
    /// Obtém todos os usuários.
    /// </summary>
    /// <returns>Um IActionResult contendo todos os usuários ou uma mensagem de erro.</returns>
    [HttpGet]
    public async Task<IActionResult> GetAllUsuarioAsync()
    {
        var userId = User.GetId();
        var usuario = await _usuarioService.GetUsuarioByIdAsync(userId);

        if (!usuario.Admin)
        {
            return Ok(await _usuarioService.GetUsuarioByIdAsync(userId));
        }
        else
        {
            var usuariosDto = await _usuarioService.GetAllUsuarioAsync();
            return Ok(usuariosDto);
        }
    }

    /// <summary>
    /// Edita um usuário existente.
    /// </summary>
    /// <param name="usuarioDto">Usuário para alterações.</param>
    /// <returns>Um IActionResult indicando sucesso ou falha na edição.</returns>
    [HttpPut]
    public async Task<IActionResult> EditUsuarioAsync(UserPutDto usuarioDto)
    {
        var userId = User.GetId();
        var user = await _usuarioService.GetUsuarioByIdAsync(userId);

        if (!user.Admin && usuarioDto.Id != userId)
        {
            return Unauthorized("Você não tem permissão para alterar os usuários do sistema.");
        }
        if (!user.Admin && usuarioDto.Id == userId && usuarioDto.Admin)
        {
            return Unauthorized("Você não tem permissão para definir você mesmo como administrador.");
        }

        var usuarioDtoEdit = await _usuarioService.EditUsuarioAsync(usuarioDto);
        if (usuarioDtoEdit == null) return BadRequest("Ocorreu um erro ao editar um usuário");
        return Ok("Usuário editado com sucesso");
    }

    /// <summary>
    /// Remove um usuário pelo ID.
    /// </summary>
    /// <param name="id">O ID do usuário a ser removido.</param>
    /// <returns>Um IActionResult indicando sucesso ou falha na remoção.</returns>
    [HttpDelete]
    public async Task<IActionResult> RemoveUsuario(int id)
    {
        var usuarioDtoRemove = await _usuarioService.RemoveUsuario(id);
        if (usuarioDtoRemove == null) return BadRequest("Ocorreu um erro ao excluir um usuário");
        return Ok("Usuário excluído com sucesso");
    }
}