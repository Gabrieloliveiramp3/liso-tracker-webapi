﻿using Castle.Components.DictionaryAdapter.Xml;
using FinançasPessoaisWebAPI.Api.Extensions;
using FinançasPessoaisWebAPI.Api.Models;
using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;
using FinançasPessoaisWebAPI.Application.DTOs.Transacao;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Ioc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using static FinançasPessoaisWebAPI.Application.DTOs.Conta.ContaDto;
using TipoTransacao = FinançasPessoaisWebAPI.Domain.Entities.Categoria.TipoTransacao;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[ApiController]
[Route("[controller]")]
[Authorize]
public class TransacaoController(ITransacaoService transacaoService, IUsuarioService usuarioService, FinançasPessoaisWebApiApiContext context, IMemoryCache cache) : Controller
{
    private readonly ITransacaoService _transacaoService = transacaoService;
    private readonly IUsuarioService _usuarioService = usuarioService;
    private readonly FinançasPessoaisWebApiApiContext _context = context;
    private readonly IMemoryCache _cache = cache;

    #region Get Id

    /// <summary>
    /// Seleciona uma transação pelo ID.
    /// </summary>
    /// <param name="id">O ID da transação a ser selecionada.</param>
    /// <returns>
    /// Retorna 'Unauthorized' se o ID não corresponder ao usuário autenticado.
    /// Retorna `NotFound` se nenhuma transação for encontrada para o usuário.
    /// Retorna `Ok` com as transações do usuário se forem encontradas.
    /// </returns>
    [HttpGet("{id}")]
    public async Task<ActionResult> GetTransacoesByUsuarioIdAsync(int id)
    {
        var userId = User.GetId();
        var transacao = await _transacaoService.GetTransacoesByUsuarioIdAsync(userId, id);

        if (transacao == null)
        {
            return NotFound("Nenhuma transação encontrada para este usuário.");
        }

        // Verifica se a transação está em cache
        if (!_cache.TryGetValue($"Transacao_{id}", out TransacaoDto transacaoDto))
        {
            // Se não está em cache, busca no serviço
            transacaoDto = await _transacaoService.GetTransacoesByUsuarioIdAsync(userId, id);

            // Armazena em cache por 10 minutos
            _cache.Set($"Transacao_{id}", transacaoDto, TimeSpan.FromMinutes(10));
        }

        if (transacao.Categoria.categoriaDespesa != null)
        {
            var transacaoDespesa = new TransacaoGetIdDespesa
            {
                Valor = transacaoDto.Valor,
                Descricao = transacaoDto.Descricao,
                CategoriaDespesa = new CategoriaDespesaDto
                {
                    tipoTransacao = (CategoriaDespesaDto.TipoTransacao)transacao.Categoria.tipoTransacao,
                    categoriaDespesa = (CategoriaDespesaDto.CategoriaDespesa)transacaoDto.Categoria.categoriaDespesa
                },
                Data = transacaoDto.Data,
                Conta = transacaoDto.Conta
            };

            return Ok(transacaoDespesa);
        }
        else if (transacao.Categoria.categoriaReceita != null)
        {
            var transacaoReceita = new TransacaoGetIdReceita
            {
                Valor = transacaoDto.Valor,
                Descricao = transacaoDto.Descricao,
                CategoriaReceita = new CategoriaReceitaDto
                {
                    tipoTransacao = (CategoriaReceitaDto.TipoTransacao)transacao.Categoria.tipoTransacao,
                    categoriaReceita = (CategoriaReceitaDto.CategoriaReceita)transacaoDto.Categoria.categoriaReceita
                },
                Data = transacaoDto.Data,
                Conta = transacaoDto.Conta
            };

            return Ok(transacaoReceita);
        }

        return BadRequest("Erro ao processar");
    }

    #endregion Get Id

    #region Get All

    /// <summary>
    /// Obtém todas as Transacoes
    /// </summary>
    /// <param name="tipoTransacao"></param>
    /// <param name="paginasParams">Paginas a serem consultadas</param>
    /// <returns></returns>
    ///
    [HttpGet]
    [Route("/{tipoTransacao}")]
    public async Task<IActionResult> GetAllTransacaoAsync(
        [FromRoute] TipoTransacao tipoTransacao,
        [FromQuery] PaginasParams paginasParams)
    {
        var userId = User.GetId();

        if (tipoTransacao == TipoTransacao.Receita)
        {
            var receitaTransacaoDto = await _transacaoService.GetAllTransacaoReceitaAsync(userId, paginasParams.NumerodaPagina, paginasParams.Quantidade);
            Response.AddPaginationHeader(new PaginasHeader(receitaTransacaoDto.CurrentPage,
                receitaTransacaoDto.Quantidade, receitaTransacaoDto.TotalCount, receitaTransacaoDto.TotalPages));
            return Ok(receitaTransacaoDto);
        }
        if (tipoTransacao == TipoTransacao.Despesa)
        {
            var despesaTransacaoDto = await _transacaoService.GetAllTransacaoDespesaAsync(userId, paginasParams.NumerodaPagina, paginasParams.Quantidade);
            Response.AddPaginationHeader(new PaginasHeader(despesaTransacaoDto.CurrentPage,
                despesaTransacaoDto.Quantidade, despesaTransacaoDto.TotalCount, despesaTransacaoDto.TotalPages));
            return Ok(despesaTransacaoDto);
        }
        return Ok(tipoTransacao);
    }

    #endregion Get All

    #region Post Receita

    /// <summary>
    /// Cria uma Transação de Receita.
    /// </summary>
    /// Retorna `BadRequest` se ocorrer um erro ao criar a transação.
    /// Retorna `Ok` com uma mensagem de sucesso se a transação for criada com sucesso.
    /// <remarks>
    /// Exemplo:
    ///
    ///     POST /Receita
    ///     {
    ///        "valor": 5000,
    ///        "descricao": "Pagamento",
    ///        "data": "2024-05-14T17:41:19.271Z"
    ///     }
    ///
    /// </remarks>
    /// <param name="categoriaReceita"></param>
    /// <param name="conta"></param>
    /// <param name="transacaoPostReceitaDto">Dados da nova transação de Receita.</param>
    /// <returns>Um novo item criado</returns>
    /// <response code="201">Retorna uma transação criada</response>
    /// <response code="401">Não Autorizado</response>
    [HttpPost]
    [Route("Receita/{categoriaReceita}/{conta}")]
    public async Task<IActionResult> CreateTransacaoReceitaAsync(
        [FromRoute] CategoriaReceitaDto.CategoriaReceita categoriaReceita,
        [FromRoute] TipoConta conta,
        [FromBody] TransacaoPostReceitaDto transacaoPostReceitaDto)
    {
        var userId = User.GetId();
        transacaoPostReceitaDto.UsuarioId = userId;
        transacaoPostReceitaDto.CategoriaReceita = new CategoriaReceitaDto()
        {
            categoriaReceita = categoriaReceita,
        };
        transacaoPostReceitaDto.Conta = new ContaDto()
        {
            tipoConta = conta
        };

        var transacaoDTOCreate = await _transacaoService.CreateTransacaoReceitasAsync(userId, transacaoPostReceitaDto);
        if (transacaoDTOCreate == null) return BadRequest("Ocorreu um erro ao criar uma transacao");
        return Ok("Transacao criada com sucesso");
    }

    #endregion Post Receita

    #region Post Despesa

    /// <summary>
    /// Cria uma Transação de Despesa.
    /// </summary>
    /// <remarks>
    /// Exemplo:
    ///
    ///     POST /Despesa
    ///     {
    ///        "usuarioId": 1,
    ///        "valor": 5000,
    ///        "descricao": "Pagamento",
    ///        "data": "2024-05-14T17:41:19.271Z"
    ///      }
    ///
    /// </remarks>
    /// <param name="categoriaDespesa"></param>
    /// <param name="conta"></param>
    /// <param name="transacaoPostDespesaDto">Dados da nova transação de Despesa</param>
    /// <returns>Um novo item criado</returns>
    /// <response code="201">Retorna uma transação criada</response>
    /// <response code="400">Se o item não for criado</response>
    [HttpPost]
    [Route("Despesa/{categoriaDespesa}/{conta}")]
    public async Task<IActionResult> CreateTransacaoDespesaAsync(
        [FromRoute] CategoriaDespesaDto.CategoriaDespesa categoriaDespesa,
        [FromRoute] TipoConta conta,
        [FromBody] TransacaoPostDespesaDto transacaoPostDespesaDto)
    {
        var userId = User.GetId();
        transacaoPostDespesaDto.UsuarioId = userId;
        transacaoPostDespesaDto.CategoriaDespesa = new CategoriaDespesaDto()
        {
            categoriaDespesa = categoriaDespesa,
        };
        transacaoPostDespesaDto.Conta = new ContaDto()
        {
            tipoConta = conta
        };
        var transacaoDTOCreate = await _transacaoService.CreateTransacaoDespesasAsync(userId, transacaoPostDespesaDto);
        if (transacaoDTOCreate == null) return BadRequest("Ocorreu um erro ao criar uma transacao");
        return Ok("Transacao criada com sucesso");
    }

    #endregion Post Despesa

    #region Put Receita

    /// <summary>
    ///     Edita uma transação de Receita.
    /// </summary>
    /// <param name="transacaoPutReceita">Dados da transação.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpPut]
    [Route("/Receita")]
    public async Task<IActionResult> EditTransacaoReceitaAsync(TransacaoPutReceita transacaoPutReceita)
    {
        var userId = User.GetId();
        var transacaoDtoEdit = await _transacaoService.EditTransacaoReceitaAsync(userId, transacaoPutReceita);
        if (transacaoDtoEdit == null) return BadRequest("Ocorreu um erro ao editar uma transacao");
        return Ok("Transacao editada com sucesso");
    }

    #endregion Put Receita

    #region Put Despesa

    /// <summary>
    ///     Edita uma transação de Despesa.
    /// </summary>
    /// <param name="transacaoDto">Dados da transação.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpPut]
    [Route("/Despesa")]
    public async Task<IActionResult> EditTransacaoDespesaAsync(TransacaoPutDespesa transacaoPutDespesa)
    {
        var userId = User.GetId();
        var transacaoDtoEdit = await _transacaoService.EditTransacaoDespesaAsync(userId, transacaoPutDespesa);
        if (transacaoDtoEdit == null) return BadRequest("Ocorreu um erro ao editar uma transacao");
        return Ok("Transacao editada com sucesso");
    }

    #endregion Put Despesa

    #region Delete

    /// <summary>
    ///     Remove uma transação pelo ID.
    /// </summary>
    /// <param name="id">ID da transação a ser deletada.</param>
    /// <returns>Mensagem de sucesso ou erro.</returns>
    [HttpDelete]
    public async Task<IActionResult> RemoveTransacao(int id)
    {
        var userId = User.GetId();
        var transacaoDtoRemove = await _transacaoService.RemoveTransacao(userId, id);
        if (transacaoDtoRemove == null) return BadRequest("Ocorreu um erro ao excluir uma transacao");
        return Ok("Transacao excluída com sucesso");
    }

    #endregion Delete
}