﻿using FinançasPessoaisWebAPI.Domain.Relatorio;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using FinançasPessoaisWebAPI.Infra.Ioc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[Route("[controller]")]
[ApiController]
[Authorize]
public class RelatorioController : Controller
{
    private readonly IRelatorioRepository _relatorioRepository;

    public RelatorioController(IRelatorioRepository relatorioRepository)
    {
        _relatorioRepository = relatorioRepository;
    }

    /// <summary>
    /// Gera um Relatório
    /// </summary>
    /// <returns>Um Relatório</returns>
    /// <response code="201">Retorna um Relatório</response>
    /// <response code="401">Não Autorizado</response>
    [HttpGet]
    public ResultadoRelatorio GetTransacaoReceitaeDespesas()
    {
        var userId = User.GetId();
        return _relatorioRepository.Relatorios(userId);
    }
}