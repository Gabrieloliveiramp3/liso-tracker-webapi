﻿using FinançasPessoaisWebAPI.Api.Middleware;
using FinançasPessoaisWebAPI.Domain.Validators;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Ioc;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Serilog;
using Serilog.Events;
using System.Globalization;
using System.Reflection;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddValidatorsFromAssemblyContaining<UsuarioValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CategoriaValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<TransacaoValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<ContaValidator>();
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddControllers().AddFluentValidation(p =>
{
    p.ValidatorOptions.LanguageManager.Culture = new CultureInfo("pt-BR");
});
builder.Services.AddMvc().AddFluentValidation(fv =>
{
    fv.ImplicitlyValidateChildProperties = true;
});

builder.Services.AddControllers();
builder.Services.AddMemoryCache();
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddInfrastructureSwagger();
builder.Services.AddInfrastructure(builder.Configuration);

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Liso Tracker WebAPI",
        Version = "v1",
        Description = " \"Se você não comprar nada, o desconto é maior.\" ",
        Contact = new OpenApiContact
        {
            Name = "Gabriel Oliveira - Jose Victor - Alessandro Beserra - Fabio Ricardo - Wana Batista",

            Url = new Uri(
                "https://gitlab.com/Gabrieloliveiramp3/liso-tracker-webapi")
        }
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    // Inclui comentários XML na documentação Swagger
    c.IncludeXmlComments(xmlPath);
});

// Configura serviços para controladores e serialização JSON
builder.Services.AddControllers().AddJsonOptions(x =>
{
    // Adiciona conversor para serializar enums como strings
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});

builder.Services.AddDbContext<FinançasPessoaisWebApiApiContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")).EnableDetailedErrors());

builder.Services.AddSqlite<FinançasPessoaisWebApiApiContext>("Data Source=FinançasPessoais.db");

//LOGS

// Verifica se o diretório de logs existe; se não, ele é criado
var logDirectory = "c:\\Logs";
if (!Directory.Exists(logDirectory))
{
    Directory.CreateDirectory(logDirectory);
}

// Verifica se o diretório de backup existe; se não, ele é criado
var backupDirectory = "c:\\Backups";
if (!Directory.Exists(backupDirectory))
{
    Directory.CreateDirectory(backupDirectory);
}

var logger = new LoggerConfiguration()
            .MinimumLevel.Verbose()
            .MinimumLevel.Override("System", LogEventLevel.Debug)
            .Destructure.ToMaximumDepth(maximumDestructuringDepth: 3) //Define a profundidade máxima de destruturação para objetos complexos. Neste caso, a profundidade máxima é 3, o que significa que objetos aninhados serão exibidos até o terceiro nível.
            .Destructure.ToMaximumStringLength(maximumStringLength: 3) //Define o comprimento máximo para strings exibidas nos logs.Se uma string for mais longa que 3 caracteres, ela será truncada.
            .Destructure.ToMaximumCollectionCount(maximumCollectionCount: 3) //Define o número máximo de elementos exibidos para coleções (como listas ou arrays) nos logs. Se uma coleção tiver mais de 3 elementos, apenas os primeiros 3 serão exibidos.
            .Destructure.AsScalar(typeof(System.Version)) //Especifica que objetos do tipo System.Version devem ser tratados como valores escalares (ou seja, exibidos diretamente como uma string).
            .Enrich.WithProperty("AppName", "MyApp")
            .Enrich.WithProperty("ServerName", "MyServer")
            .WriteTo.Console()
            .WriteTo.File(
                Path.Combine(logDirectory, "FinancasWebAPI-.log"), // Caminho completo para o arquivo de log
                fileSizeLimitBytes: 10485760, // Limite de tamanho do arquivo de log (10 MB)
                rollOnFileSizeLimit: true, // Quando definida como true, o Serilog irá automaticamente criar um novo arquivo de log quando o tamanho do arquivo atual atingir um limite predefinido. Isso evita que o arquivo de log cresça indefinidamente e ocupe muito espaço de armazenamento.
                restrictedToMinimumLevel: LogEventLevel.Debug,
                rollingInterval: RollingInterval.Day, // Rotação diária - significa que o arquivo de log será rotacionado (ou seja, um novo arquivo será criado) diariamente.
                retainedFileCountLimit: 5) // Manter 5 arquivos de log
            .CreateLogger();

builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);

// Lógica de backup e exclusão de arquivos antigos
foreach (var logFile in Directory.GetFiles(logDirectory))
{
    var fileInfo = new FileInfo(logFile);

    // Se o arquivo tiver mais de 3 dias, faz o backup
    if (fileInfo.LastWriteTimeUtc < DateTime.UtcNow.AddDays(-3))
    {
        string backupPath = Path.Combine(backupDirectory, fileInfo.Name);
        File.Copy(logFile, backupPath, true); // Copia o arquivo para o diretório de backup
        File.Delete(logFile); // Exclui o arquivo original
    }
}

//##############################

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI(
   c =>
   {
       c.SwaggerEndpoint("/swagger/v1/swagger.json", "Liso Tracker WebAPI V.1");
       c.DefaultModelsExpandDepth(-1); // define a profundidade de expansão padrão para -1, o que efetivamente oculta todos os modelos (ou esquemas) na interface do Swagger UI.
   });

app.UseMiddleware<ExceptionMiddleware>();
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();