﻿using FinançasPessoaisWebAPI.Api.Models;
using System.Text.Json;

namespace FinançasPessoaisWebAPI.Api.Extensions;

public static class HttpExtensions
{
    // Método de extensão para adicionar cabeçalhos de paginação à resposta HTTP
    public static void AddPaginationHeader(this HttpResponse response, PaginasHeader header)
    {
        // Configura as opções do serializador JSON para usar CamelCase
        var jsonOptions = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase };

        // Adiciona um cabeçalho 'Pagination' com as informações de paginação serializadas em JSON
        response.Headers.Add("Pagination", JsonSerializer.Serialize(header, jsonOptions));

        // Adiciona um cabeçalho 'Access-Control-Expose-Headers' para permitir que o cabeçalho 'Pagination' seja acessado pelo usuário
        response.Headers.Add("Access-Control-Expose-Headers", "Pagination");
    }
}