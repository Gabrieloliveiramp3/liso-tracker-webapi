﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Api.Models;

public class Login
{
    [Required(ErrorMessage = "0 e-mail e obrigatório")]
    [DataType(DataType.EmailAddress)]
    public string Email { get; set; } = null!;

    [Required(ErrorMessage = "A senha e obrigatória")]
    [DataType(DataType.Password)]
    [MinLength(10, ErrorMessage = "A senha deve ter, no minimo, 10 caracteres.")]
    public string Senha { get; set; }
}