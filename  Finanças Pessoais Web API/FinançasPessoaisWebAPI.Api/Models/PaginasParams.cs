﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Api.Models;

public class PaginasParams
{
    [Range(1, int.MaxValue)]
    public int NumerodaPagina { get; set; }

    [Range(1, 50, ErrorMessage = "O máximo de itens por página são 50.")]
    public int Quantidade { get; set; }
}