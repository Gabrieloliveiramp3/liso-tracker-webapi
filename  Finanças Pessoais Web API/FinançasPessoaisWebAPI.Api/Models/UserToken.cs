﻿namespace FinançasPessoaisWebAPI.Api.Models;

public class UserToken
{
    public string Mensagem { get; set; }
    public string Token { get; set; }
    public object Admin { get; set; }
}