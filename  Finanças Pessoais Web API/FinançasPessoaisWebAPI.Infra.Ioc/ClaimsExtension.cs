﻿using System.Security.Claims;

namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class ClaimsExtension
{
    // Obtém o ID do usuário a partir das reivindicações (claims)
    public static int GetId(this ClaimsPrincipal user)
    {
        if (user.Identity.IsAuthenticated)
        {
            // Procura a reivindicação chamada "id"
            var claim = user.FindFirst("id");
            if (claim != null && int.TryParse(claim.Value, out int id))
            {
                // Retorna o ID convertido
                return id;
            }
        }

        // Lança uma exceção se o usuário não estiver autenticado ou a reivindicação não for válida
        throw new InvalidOperationException("Usuário não encontrado ou não autenticado.");
    }

    // Obtém o e-mail do usuário a partir das reivindicações (claims)
    public static string GetEmail(this ClaimsPrincipal user)
    {
        // Procura a reivindicação chamada "email"
        var claim = user.FindFirst("email");
        // Retorna o valor da reivindicação de e-mail ou uma string vazia se não existir
        return claim?.Value ?? string.Empty;
    }
}

/*As claims ou afirmações, são uma forma de conceder os acessos baseados em dados
  imperativos como por exemplo uma data de aniversário.*/