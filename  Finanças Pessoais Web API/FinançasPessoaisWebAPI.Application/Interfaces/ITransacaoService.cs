﻿using FinançasPessoaisWebAPI.Application.DTOs.Transacao;
using FinançasPessoaisWebAPI.Domain.Paginacao;

namespace FinançasPessoaisWebAPI.Application.Interfaces;

public interface ITransacaoService
{
    Task<TransacaoPostReceitaDto> CreateTransacaoReceitasAsync(int userId, TransacaoPostReceitaDto transacaoReceita);

    Task<TransacaoPostDespesaDto> CreateTransacaoDespesasAsync(int userId, TransacaoPostDespesaDto transacaoDespesa);

    Task<TransacaoPutReceita> EditTransacaoReceitaAsync(int userId, TransacaoPutReceita transacaoPutReceita);

    Task<TransacaoPutDespesa> EditTransacaoDespesaAsync(int userId, TransacaoPutDespesa transacaoPutDespesa);

    Task<TransacaoDto> RemoveTransacao(int userId, int id);

    Task<Paginas<TransacaoGetAllReceita>> GetAllTransacaoReceitaAsync(int userId, int pageNumber, int pageSize);

    Task<Paginas<TransacaoGetAllDespesa>> GetAllTransacaoDespesaAsync(int userId, int pageNumber, int pageSize);

    Task<TransacaoDto> GetTransacoesByUsuarioIdAsync(int userId, int Id);
}