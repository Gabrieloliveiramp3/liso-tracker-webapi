﻿using FinançasPessoaisWebAPI.Application.DTOs.Categoria;

namespace FinançasPessoaisWebAPI.Application.Interfaces

{
    public interface ICategoriaService
    {
       
        Task<IEnumerable<CategoriaDespesaDTO>> GetAllCategoriasReceitaAsync();

        Task<IEnumerable<CategoriaDespesaDTO>> GetAllCategoriasDespesaAsync();

        Task<CategoriaDTO> GetCategoriaByIdAsync(int id);
    }
}