﻿using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;
using FinançasPessoaisWebAPI.Application.DTOs.Transacao;
using FinançasPessoaisWebAPI.Application.DTOs.Usuario;
using FinançasPessoaisWebAPI.Domain.Entities;

namespace FinançasPessoaisWebAPI.Application.Mappings;

// Classe que define o mapeamento entre as entidades e os DTOs

public class EntitiesDtoProfile : Profile
{
    public EntitiesDtoProfile()

    {
        //TRANSAÇÃO
        // Mapeia a entidade Transacao para o DTO TransacaoDTO e vice-versa
        CreateMap<Transacao, TransacaoDto>().ReverseMap()
            .ForPath(dest => dest.Categoria.tipoTransacao, opt => opt.MapFrom(src => src.Categoria.tipoTransacao))
           .ForPath(dest => dest.Categoria.categoriaReceita, opt => opt.MapFrom(src => src.Categoria.categoriaReceita))
           .ForPath(dest => dest.Categoria.categoriaDespesa, opt => opt.MapFrom(src => src.Categoria.categoriaDespesa));

        CreateMap<TransacaoDto, Transacao>().ReverseMap()
            .ForPath(dest => dest.Conta.tipoConta, opt => opt.MapFrom(src => src.Categoria.tipoTransacao))
           .ForPath(dest => dest.Categoria.categoriaReceita, opt => opt.MapFrom(src => src.Categoria.categoriaReceita))
           .ForPath(dest => dest.Categoria.categoriaDespesa, opt => opt.MapFrom(src => src.Categoria.categoriaDespesa));

        CreateMap<TransacaoGetAllDespesa, Transacao>().ReverseMap()
        .ForPath(dest => dest.CategoriaDespesa.categoriaDespesa, opt => opt.MapFrom(src => src.Categoria.categoriaDespesa));

        CreateMap<TransacaoGetAllReceita, Transacao>().ReverseMap()
        .ForPath(dest => dest.CategoriaReceita.categoriaReceita, opt => opt.MapFrom(src => src.Categoria.categoriaReceita));

        CreateMap<TransacaoGetIdDespesa, Transacao>().ReverseMap()
        .ForPath(dest => dest.CategoriaDespesa.categoriaDespesa, opt => opt.MapFrom(src => src.Categoria.categoriaDespesa));

        CreateMap<TransacaoGetIdReceita, Transacao>().ReverseMap()
        .ForPath(dest => dest.CategoriaReceita.categoriaReceita, opt => opt.MapFrom(src => src.Categoria.categoriaReceita));

        CreateMap<Transacao, TransacaoPostReceitaDto>().ReverseMap();
        CreateMap<TransacaoPostReceitaDto, Transacao>()
           .ForPath(dest => dest.Categoria.tipoTransacao, opt => opt.MapFrom(src => src.CategoriaReceita.tipoTransacao))
           .ForPath(dest => dest.Categoria.categoriaReceita, opt => opt.MapFrom(src => src.CategoriaReceita.categoriaReceita));

        CreateMap<Transacao, TransacaoPostDespesaDto>().ReverseMap();
        CreateMap<TransacaoPostDespesaDto, Transacao>()
          .ForPath(dest => dest.Categoria.tipoTransacao, opt => opt.MapFrom(src => src.CategoriaDespesa.tipoTransacao))
          .ForPath(dest => dest.Categoria.categoriaDespesa, opt => opt.MapFrom(src => src.CategoriaDespesa.categoriaDespesa));

        CreateMap<Transacao, TransacaoPutDespesa>().ReverseMap()
            .ForPath(dest => dest.Categoria.categoriaDespesa, opt => opt.MapFrom(src => src.CategoriaputDespesa.categoriaDespesa))
           .ForPath(dest => dest.Conta.tipoConta, opt => opt.MapFrom(src => src.ContaDto.tipoConta));

        CreateMap<Transacao, TransacaoPutReceita>().ReverseMap()
            .ForPath(dest => dest.Categoria.categoriaReceita, opt => opt.MapFrom(src => src.CategoriaPutReceita.categoriaReceita))
           .ForPath(dest => dest.Conta.tipoConta, opt => opt.MapFrom(src => src.ContaDto.tipoConta));

        //CATEGORIA
        // Mapeia a entidade Categoria para o DTO CategoriaDto e vice-versa
        CreateMap<Categoria, CategoriaReceitaDto>().ReverseMap();
        CreateMap<Categoria, CategoriaDespesaDto>().ReverseMap();
        CreateMap<Categoria, CategoriaPutDespesa>().ReverseMap();
        CreateMap<Categoria, CategoriaPutReceita>().ReverseMap();

        //CONTA

        CreateMap<Conta, ContaDto>().ReverseMap();

        //USUÁRIO
        CreateMap<Usuario, UsuarioDto>().ReverseMap();
        CreateMap<Usuario, UserGetAll>().ReverseMap();
        CreateMap<UsuarioDto, Usuario>().ReverseMap();
        CreateMap<Usuario, UsuarioDto>().ReverseMap()
          .ForMember(t => t.SenhaNormal, opt => opt.MapFrom(x => x.Senha));

        CreateMap<UsuarioDto, Usuario>().ReverseMap()
          .ForMember(t => t.Senha, opt => opt.MapFrom(x => x.SenhaNormal));
        CreateMap<Usuario, UsuarioRegisterDto>().ReverseMap();
        CreateMap<Usuario, UserPutDto>().ReverseMap();
        CreateMap<Usuario, UserView>().ReverseMap();
    }
}

/*
- AutoMapper é uma biblioteca que facilita o mapeamento entre objetos de diferentes tipos.
- A classe EntitiesDTOProfile herda da classe Profile do AutoMapper.
- No construtor dessa classe, são definidos os mapeamentos entre as entidades e os DTOs.
- CreateMap<Transacao, TransacaoDTO>() cria um mapeamento da entidade Transacao para o DTO TransacaoDTO.
- ReverseMap() permite que o mapeamento seja bidirecional (ou seja, também do DTO para a entidade).

Essa classe é útil para manter a separação entre as entidades do banco de dados e os objetos de transferência de dados(DTOs) usados na API.O AutoMapper simplifica a conversão entre esses tipos, economizando código repetitivo.*/