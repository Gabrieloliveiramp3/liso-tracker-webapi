using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs.Usuario;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Account;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using System.Security.Cryptography;
using System.Text;

namespace FinançasPessoaisWebAPI.Application.Services;

public class UsuarioService : IUsuarioService
{
    private readonly IUsuarioRepository _usuarioRepository;
    private readonly IMapper _mapper;
    private readonly IAuthenticate _authenticateService;

    public UsuarioService(IUsuarioRepository usuarioRepository, IMapper mapper, IAuthenticate authenticateService)
    {
        _usuarioRepository = usuarioRepository;
        _mapper = mapper;
        _authenticateService = authenticateService;
    }

    public async Task<IEnumerable<UserGetAll>> GetAllUsuarioAsync()
    {
        var usuario = await _usuarioRepository.GetAllUsuarioAsync();
        return _mapper.Map<IEnumerable<UserGetAll>>(usuario);
    }

    public async Task<UsuarioDto> GetUsuarioByIdAsync(int id)
    {
        var usuario = await _usuarioRepository.GetUsuarioByIdAsync(id);
        return _mapper.Map<UsuarioDto>(usuario);
    }

    public async Task<UsuarioDto> CreateUsuarioAsync(UsuarioRegisterDto usuarioRegisterDto)
    {
        var usuario = _mapper.Map<Usuario>(usuarioRegisterDto);
        if (usuarioRegisterDto.Senha != null)
        {
            using var hmac = new HMACSHA512();
            byte[] senhaHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(usuarioRegisterDto.Senha));
            byte[] senhaNormal = hmac.Key;

            usuario.AlterarSenha(senhaNormal, senhaHash);
        }
        var usuarioCriado = await _usuarioRepository.CreateUsuarioAsync(usuario);
        return _mapper.Map<UsuarioDto>(usuarioCriado);
    }

    public async Task<UserPutDto> EditUsuarioAsync(UserPutDto usuarioDto)
    {
        var usuario = _mapper.Map<Usuario>(usuarioDto);

        if (usuarioDto.Senha != null)
        {
            using var hmac = new HMACSHA512();
            byte[] senhaHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(usuarioDto.Senha));
            byte[] senhaNormal = hmac.Key;

            usuario.AlterarSenha(senhaNormal, senhaHash);
        }
        var usuarioEditada = await _usuarioRepository.EditUsuarioAsync(usuario);
        return _mapper.Map<UserPutDto>(usuarioEditada);
    }

    public async Task<UserView> RemoveUsuario(int id)
    {
        var usuarioExcluida = await _usuarioRepository.RemoveUsuario(id);
        return _mapper.Map<UserView>(usuarioExcluida);
    }

    public async Task<bool> UserExiste()
    {
        return await _usuarioRepository.UserExiste();
    }
}