﻿using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Interfaces;

namespace FinançasPessoaisWebAPI.Application.Services.CategoriaService;

public class CategoriaService : ICategoriaService
{
    private readonly ICategoriaRepository _categoriaRepository;
    private readonly IMapper _mapper;

    public CategoriaService(ICategoriaRepository categoriaRepository, IMapper mapper)
    {
        _categoriaRepository = categoriaRepository;
        _mapper = mapper;
    }

    public async Task<CategoriaDTO> CreateCategoriaAsync(CategoriaDTO categoriaDTO)
    {
        var categoria = _mapper.Map<Categoria>(categoriaDTO);
        var categoriaCriada = await _categoriaRepository.CreateCategoriaAsync(categoria);
        return _mapper.Map<CategoriaDTO>(categoriaCriada);
    }

    public async Task<CategoriaDTO> EditCategoriaAsync(CategoriaDTO categoriaDTO)
    {
        var categoria = _mapper.Map<Categoria>(categoriaDTO);
        var categoriaEditada = await _categoriaRepository.EditCategoriaAsync(categoria);
        return _mapper.Map<CategoriaDTO>(categoriaEditada);
    }

    public async Task<IEnumerable<CategoriaDTO>> GetAllCategoriaAsync()
    {
        var categoria = await _categoriaRepository.GetAllCategoriaAsync();
        return _mapper.Map<IEnumerable<CategoriaDTO>>(categoria);
    }

    public async Task<IEnumerable<CategoriaDespesaDTO>> GetAllCategoriasDespesaAsync()
    {
        var categoria = await _categoriaRepository.GetAllCategoriasDespesaAsync();
        return _mapper.Map<IEnumerable<CategoriaDespesaDTO>>(categoria);
    }

    public async Task<IEnumerable<CategoriaDespesaDTO>> GetAllCategoriasReceitaAsync()
    {
        var categoria = await _categoriaRepository.GetAllCategoriasReceitaAsync();
        return _mapper.Map<IEnumerable<CategoriaDespesaDTO>>(categoria);
    }

    public async Task<CategoriaDTO> RemoveCategoria(int id)
    {
        var categoriaExcluida = await _categoriaRepository.RemoveCategoria(id);
        return _mapper.Map<CategoriaDTO>(categoriaExcluida);
    }

    async Task<CategoriaDTO> ICategoriaService.GetCategoriaByIdAsync(int id)
    {
        var categoria = await _categoriaRepository.GetCategoriaByIdAsync(id);
        return _mapper.Map<CategoriaDTO>(categoria);
    }
}