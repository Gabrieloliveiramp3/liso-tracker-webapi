﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Categoria;

public class CategoriaId
{
    public TipoTransacao tipoTransacao { get; set; }
    public CategoriaReceita? categoriaReceita { get; set; }
    public CategoriaDespesa? categoriaDespesa { get; set; }

    public enum TipoTransacao
    {
        Receita,
        Despesa
    }

    public enum CategoriaReceita
    {
        Outros,
        Salário,
        Benefícios,
        Vendas,
        Economias
    }

    public enum CategoriaDespesa
    {
        Outros,
        Saúde,
        Aluguel,
        Educação,
        Lazer,
        Alimentação,
        DespesasPessoais
    }
}