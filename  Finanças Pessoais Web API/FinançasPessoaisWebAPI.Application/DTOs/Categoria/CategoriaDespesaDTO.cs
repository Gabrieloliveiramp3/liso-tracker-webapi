﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Categoria;

public class CategoriaDespesaDto
{
    public TipoTransacao tipoTransacao { get; set; }

    public CategoriaDespesa categoriaDespesa { get; set; }

    public enum CategoriaDespesa
    {
        Outros,
        Saúde,
        Aluguel,
        Educação,
        Lazer,
        Alimentação,
        DespesasPessoais
    }

    public enum TipoTransacao
    {
        Despesa
    }
}