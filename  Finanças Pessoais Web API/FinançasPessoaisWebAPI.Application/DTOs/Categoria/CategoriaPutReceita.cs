﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Categoria;

public class CategoriaPutReceita
{
    public CategoriaReceita categoriaReceita { get; set; }

    public enum CategoriaReceita
    {
        Outros,
        Salário,
        Benefícios,
        Vendas,
        Economias
    }
}