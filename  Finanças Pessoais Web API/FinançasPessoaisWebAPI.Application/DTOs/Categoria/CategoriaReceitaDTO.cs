﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Categoria;

public class CategoriaReceitaDto

{
    public TipoTransacao tipoTransacao { get; set; }

    public CategoriaReceita categoriaReceita { get; set; }

    public enum CategoriaReceita
    {
        Outros,
        Salário,
        Benefícios,
        Vendas,
        Economias
    }

    public enum TipoTransacao
    {
        Receita
    }
}