﻿using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;

namespace FinançasPessoaisWebAPI.Application.DTOs.Transacao;

public class TransacaoGetAllDespesa
{
    public int Id { get; set; }
    public decimal Valor { get; set; }
    public string Descricao { get; set; }
    public DateTime Data { get; set; } = DateTime.Now;
    public virtual CategoriaDespesaDto CategoriaDespesa { get; set; }
    public virtual ContaDto Conta { get; set; }
}