﻿using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FinançasPessoaisWebAPI.Application.DTOs.Transacao;

public class TransacaoPostDespesaDto
{
    [JsonIgnore]
    public int UsuarioId { get; set; }

    [JsonIgnore]
    public int Id { get; set; }

    [JsonIgnore]
    public int ContaId { get; set; }

    [JsonIgnore]
    public int CategoriaId { get; set; }

    [Required(ErrorMessage = "O valor é obrigatório.")]
    [Range(1, 999999, ErrorMessage = "Valor deve ser maior que zero")]
    public decimal Valor { get; set; }

    [MaxLength(50, ErrorMessage = "O campo Descrição pode até 50 caracteres")]
    public string Descricao { get; set; }

    [Range(typeof(DateTime), "2000-05-14T17:41:19.271Z", "3000-05-13T18:42:17.271Z",
     ErrorMessage = "Data no Formato incorreto")]
    public DateTime Data { get; set; } = DateTime.Now;

    [JsonIgnore]
    public virtual CategoriaDespesaDto CategoriaDespesa { get; set; }

    [JsonIgnore]
    public virtual ContaDto Conta { get; set; }
}