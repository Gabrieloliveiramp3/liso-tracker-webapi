﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Conta;

public class ContaDto
{
    public TipoConta tipoConta { get; set; }

    public enum TipoConta
    {
        Carteira,
        ContaCorrente,
        Poupanca
    }
}