﻿using FinançasPessoaisWebAPI.FrontEnd.Models;
using FinançasPessoaisWebAPI.FrontEnd.Services;
using System.Data;
using System.Globalization;
using System.Reflection.Metadata.Ecma335;

namespace FinançasPessoaisWebAPI.FrontEnd.Helpers
{
    public class TransacoesOV
    {
        private TransactionService TransactionService { get; set; }

        // CONSTRUTOR DEVE RECEBER UMA TRANSACTION SERVICE PARA PODER FAZER REQUISIÇÕES
        public TransacoesOV(TransactionService transactionService)
        {
            TransactionService = transactionService;
        }

        private DateTime dateNow = DateTime.Today;
        private List<TransacaoBD> Transacoes = new List<TransacaoBD>();
        
        // MÉTODO PRINCIPAL QUE RETORNA TODAS AS TRANSAÇÕES DO SERVIDOR E ADICIONA AS VITUAIS (RECORRENTES)
        public async Task<List<TransacaoBD>> TransacoesTotais()
        {
            // PEGA TODOS OS DADOS DO SERVIDOR
            await GetAllTransactions();

            // ADICIONA AS TRANSAÇÕES VIRTUAIS (ADVINDAS DA RECORRÊNCIA)
            TransacoesVituais();

            // RETORNA TODAS AS TRANSAÇÕES
            return Transacoes;
        }

        // RECEBE TODAS AS TRANSAÇÕES DO SERVIDOR
        private async Task GetAllTransactions()
        {

            // RECEBE TODAS AS RECEITAS
            await GetAllTransactionsType(true);

            // RECEBE TODAS OS DESPESAS
            await GetAllTransactionsType(false);

        }

        // RECEBE TODAS AS TRANSAÇÕES DE RECEITA OU DESPESA DO SERVIDOR
        private async Task GetAllTransactionsType(bool transactionType)
        {
            var pageNumber = 1;
            var transactionTypeStr = transactionType ? "Receita" : "Despesa";
            List<TransacaoDTOR> response = null;

            while (true)
            {
                response = await TransactionService.ReceberTransacoes($"http://localhost:5299/{transactionTypeStr}?NumerodaPagina={pageNumber}&Quantidade=50");

                if (response.Count > 0 && response != null)
                {
                    var j = 0;
                    foreach (TransacaoDTOR i in response)
                    {
                        var textoChip = "";
                        if (i.recorrencia > 0)
                        {
                            switch (i.recorrenciaModo)
                            {
                                case 0:
                                    textoChip = "1/♾️";
                                    break;

                                case 1:
                                    textoChip = "ATÉ " + i.recorrenciaDataLimite.ToString("dd/MM/yy");
                                    break;

                                case 2:
                                    textoChip = "1/" + i.recorrenciaNVezes;
                                    break;

                                default:
                                    ;
                                    break;
                            }
                        }
                        var transacao = new TransacaoBD(
                            id: (int)i.id,
                            valor: (decimal)i.valor,
                            descricao: (string)i.descricao,
                            datatransa: (DateTime)i.data,
                            conta: (string)i.conta.tipoConta,
                            tipo: transactionType,
                            categoriaId: (string)(transactionType ? i.categoriaReceita.categoriaReceita : i.categoriaDespesa.categoriaDespesa),
                            recorrencia: (int)i.recorrencia,
                            recorrencia_modo: (int)i.recorrenciaModo,
                            recorrencia_n_vezes: (int)i.recorrenciaNVezes,
                            recorrencia_data_limite: (DateTime)i.recorrenciaDataLimite,
                            textoChip: textoChip
                        );
                        j++;
                        Transacoes.Add(transacao);
                    }
                    response.Clear();
                    pageNumber++;
                }
                else
                {
                    break;
                }

            }

        }

        // ADICIONA AS TRANSAÇÕES VIRTUAIS
        private void TransacoesVituais()
        {
            DateTime DATA;
            List<TransacaoBD> TransacoesOG = new List<TransacaoBD>(Transacoes);
            var TextoRecorrencia = "";

            // PERCORRE TODAS AS TRANSAÇÕES DO SERVIDOR
            foreach (TransacaoBD i in TransacoesOG)
            {
                // VERIFICA AS QUE SÃO RECORRENTES
                if (i.Recorrencia > 0)
                {
                    // ADICIONA NOVAS TRANSAÇÕES A DEPENDER DO MODO
                    switch (i.Recorrencia_Modo)
                    {
                        // INDEFINIDADMENTE (ADICIONA ATÉ CHEGAR NA DATA ATUAL)
                        case 0:
                            var count = 2;
                            TextoRecorrencia = "/♾️";
                            for (DATA = AddTempoTipo(i.Data, i.Recorrencia); DATA <= dateNow; DATA = AddTempoTipo(DATA, i.Recorrencia))
                            {
                                CriaTransacaoVirtual(i, DATA, count + TextoRecorrencia);
                                count++;
                            }
                            break;

                        // ATÉ A DATA (ADICIONA ATÉ CHEGAR NA DATA DEFINIDA - LIMITANDO-SE A DATA ATUAL)
                        case 1:
                            TextoRecorrencia = "ATÉ " + i.Recorrencia_Data_Limite.ToString("dd/MM/yy");
                            DateTime DataLimite = (i.Recorrencia_Data_Limite < dateNow) ? i.Recorrencia_Data_Limite : dateNow;
                            for (DATA = AddTempoTipo(i.Data, i.Recorrencia); DATA <= DataLimite; DATA = AddTempoTipo(DATA, i.Recorrencia))
                            {
                                CriaTransacaoVirtual(i, DATA, TextoRecorrencia);
                            }
                            break;

                        // N VEZES (N-1, JÁ QUE CONTA-SE TRANSAÇÃO ORIGINAL, ATÉ A DATA ATUAL OU ATÉ O NÚMERO ESTIPULADO)
                        case 2:
                            var countt = 2;
                            TextoRecorrencia = "/" + i.Recorrencia_N_Vezes;
                            for (DATA = AddTempoTipo(i.Data, i.Recorrencia); DATA <= dateNow; DATA = AddTempoTipo(DATA, i.Recorrencia))
                            {
                                // SE CHEGAR AO LIMITE DE REPETIÇÕES, INTERROMPE
                                if (countt > i.Recorrencia_N_Vezes) break;
                                CriaTransacaoVirtual(i, DATA, countt + TextoRecorrencia);
                                countt++;
                            }
                            break;

                        default:
                            break;
                    }
                }

            }
        }

        // CRIA UMA TRANSAÇÃO VIRTUAL BASEADA NA TRANSAÇÃO ORIGINAL, COM DATA ESPECÍFICA E TEXTO CUSTOMIZADO
        private void CriaTransacaoVirtual(TransacaoBD i, DateTime data, string textoChip)
        {
            var transacao = new TransacaoBD(
                            id: -1,
                            valor: i.Valor,
                            descricao: i.Descricao,
                            datatransa: data,
                            conta: i.ContaId,
                            tipo: i.Tipo,
                            categoriaId: i.Categoria,
                            recorrencia: i.Recorrencia,
                            recorrencia_modo: i.Recorrencia_Modo,
                            recorrencia_n_vezes: i.Recorrencia_N_Vezes,
                            recorrencia_data_limite: i.Recorrencia_Data_Limite,
                            textoChip: textoChip
                        );

            Transacoes.Add(transacao);
        }

        // ADICIONA TEMPO BASEADO NA FREQUÊNCIA (USADO NO FOR LOOP)
        private DateTime AddTempoTipo(DateTime DATA, int Modo)
        {
            switch (Modo)
            {
                // DIÁRIO
                case 1:
                    DATA = DATA.AddDays(1);
                    break;

                // SEMANAL
                case 2:
                    DATA = DATA.AddDays(7);
                    break;

                // MENSAL
                case 3:
                    DATA = DATA.AddMonths(1);
                    break;

                // ANUAL
                case 4:
                    DATA = DATA.AddYears(1);
                    break;
                default:
                    Console.WriteLine("");
                    break;
            }
            return DATA;
        }




}
}
