using System.Text.RegularExpressions;
using MudBlazor;
using System.Security.Cryptography;
using System.Text;
using Blazored.LocalStorage;
using System.Globalization;
using FinançasPessoaisWebAPI.FrontEnd.Services;

// FUNÇÕES AUXILIARES --------------------------------------------------------------
namespace FinançasPessoaisWebAPI.FrontEnd.Helpers
{

    public static class AuxFunc
    {

        // FORMATA O VALOR DECIMAL PARA FORMATO MONETÁRIO
        public static string Valor(decimal Valor, string currencyConfig)
        {
            return Valor.ToString("C", new CultureInfo(currencyConfig));
        }

        // VALIDAR EMAIL
        public static bool ValidarEmail(string emailAddress)
        {
            var pattern = @"^[a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$";
            var regex = new Regex(pattern);
            return regex.IsMatch(emailAddress);
        }

        // VERIFICA SE STRING TEM CARACTERES ESPECIAIS
        public static bool HasSpecialChars(string yourString)
        {
            return yourString.Any(ch => !char.IsLetterOrDigit(ch));
        }


        // APRESENTAÇÃO DE DADOS --------------------------------------------------------------

        // RETORNA COM O NOME DA CONTA DADO O ID DELA
        public static string NomeConta(int IdConta)
        {
            switch (IdConta)
            {

                case 0:
                    return "Conta Corrente";

                case 1:
                    return "Conta Poupança";

                case 2:
                    return "Carteira";

                default:
                    return "Conta Inexistente";
            }
        }

        // RETORNA O ÍCONE (MUD ICON) DA CONTA DADO O ID DELA
        public static string IconeConta(int IdConta)
        {
            switch (IdConta)
            {

                case 0:
                    return Icons.Material.Filled.AccountBalanceWallet;

                case 1:
                    return Icons.Material.Filled.Savings;

                case 2:
                    return Icons.Material.Filled.Wallet;

                default:
                    return Icons.Material.Filled.Block;
            }
        }

        // RETORNA UMA COR ESPECÍFICA PARA O TIPO DE TRANSAÇÃO (RECEITA | DESPESA)
        public static Color CorTransacao(bool TipoTransacao)
        {
            if (TipoTransacao)
            {
                return Color.Success;
            }
            return Color.Error;
        }

        // RETORNA UMA COR (STRING CSS) ESPECÍFICA PARA O TIPO DE TRANSAÇÃO (RECEITA | DESPESA)
        public static string CorTransacaoString(bool TipoTransacao)
        {
            if (TipoTransacao)
            {
                return "var(--mud-palette-success)";
            }
            return "var(--mud-palette-error)";
        }

        //----------------------------------------------

        // RETORNA UM ÍCONE ESPECÍFICO DADO O ID DA CATEGORIA DA TRANSAÇÃO
        public static string IconeCategoria(int IdCategoria)
        {
            switch (IdCategoria)
            {
                case 1:
                    return Icons.Material.Filled.Work;
                case 2:
                    return Icons.Material.Filled.Payments;
                case 3:
                    return Icons.Material.Filled.PointOfSale;
                case 4:
                    return Icons.Material.Filled.Savings;
                case 5:
                    return Icons.Material.Filled.Spoke;
                case 6:
                    return Icons.Material.Filled.LocalHospital;
                case 7:
                    return Icons.Material.Filled.House;
                case 8:
                    return Icons.Material.Filled.School;
                case 9:
                    return Icons.Material.Filled.BeachAccess;
                case 10:
                    return Icons.Material.Filled.LocalDining;
                case 11:
                    return Icons.Material.Filled.AccountBox;
                default:
                    return Icons.Material.Filled.Spoke;
            }
        }

        // FORMATA O CAMPO 'CATEGORIA', ALTERA O TEXTO DE ACORDO COM O CÓDIGO
        public static string CategoriaTexto(int IdCategoria)
        {
            switch (IdCategoria)
            {
                case 1:
                    return "Salário";
                case 2:
                    return "Benefícios";
                case 3:
                    return "Vendas";
                case 4:
                    return "Economias";
                case 5:
                    return "Outros";
                case 6:
                    return "Saúde";
                case 7:
                    return "Aluguel";
                case 8:
                    return "Educação";
                case 9:
                    return "Lazer";
                case 10:
                    return "Alimentação";
                case 11:
                    return "Despesas Pessoais";
                default:
                    return "???";
            }
        }

        // FORMATA O CAMPO 'CATEGORIA', ADICIONA O ÍCONE DE ACORDO COM O CÓDIGO
        public static string CategoriaIcone(int IdCategoria)
        {
            switch (IdCategoria)
            {
                case 0:
                    // Salário
                    return @Icons.Material.Filled.LocalAtm;
                case 1:
                    // Benefícios
                    return @Icons.Material.Filled.LocalAtm;
                case 2:
                    // Saúde
                    return @Icons.Material.Filled.LocalHospital;
                case 3:
                    // Alimentação
                    return @Icons.Material.Filled.LocalDining;
                case 4:
                    // Compras
                    return @Icons.Material.Filled.ShoppingBag;
                case 5:
                    // Outros
                    return @Icons.Material.Filled.Spoke;
                default:
                    return "???";
            }
        }

        public static string CategoriaCorString(int IdCategoria)
        {
            switch (IdCategoria)
            {
                case 1:
                    return "#26A69A";
                case 2:
                    return "#29B6F6";
                case 3:
                    return "#5C6BC0";
                case 4:
                    return "#78909C";
                case 5:
                    return "#5C6BC0";
                case 6:
                    return "#EF5350";
                case 7:
                    return "#66BB6A";
                case 8:
                    return "#FFA726";
                case 9:
                    return "#26C6DA";
                case 10:
                    return "#AB47BC";
                case 11:
                    return "#8D6E63";
                default:
                    return "#5C6BC0";
            }
            
        }

        //----------------------------------------------

        // RETORNA UM ÍCONE ESPECÍFICO DADO O ID DA CATEGORIA DA TRANSAÇÃO
        public static string IconeCategoria2(int IdCategoria)
        {
            switch (IdCategoria)
            {
                case 0:
                    // Salário
                    return Icons.Material.Filled.LocalAtm;
                case 1:
                    // Benefícios
                    return Icons.Material.Filled.LocalAtm;
                case 2:
                    // Saúde
                    return Icons.Material.Filled.LocalHospital;
                case 3:
                    // Alimentação
                    return Icons.Material.Filled.LocalDining;
                case 4:
                    // Compras
                    return Icons.Material.Filled.ShoppingBag;
                case 5:
                    // Outros
                    return Icons.Material.Filled.Spoke;
                default:
                    return "???";
            }
        }

        // FORMATA O CAMPO 'RECORRÊNCIA' DE ACORDO COM O CÓDIGO
        public static string Recorrencia(int Recorrencia, int Recorrencia_Modo, int Recorrencia_N_Vezes, DateTime Recorrencia_DataLimite)
        {
            string textoRetorno1 = "";
            string textoRetorno2 = "";


            switch (Recorrencia)
            {
                case 0:
                    textoRetorno1 = "Nenhuma";
                    return textoRetorno1;
                    break;

                case 1:
                    textoRetorno1 = "Diária";
                    break;

                case 2:
                    textoRetorno1 = "Semanal";
                    break;

                case 3:
                    textoRetorno1 = "Mensal";
                    break;

                case 4:
                    textoRetorno1 = "Anual";
                    break;

                default:
                    textoRetorno1 = "???";
                    break;
            }


            switch (Recorrencia_Modo)
            {
                case 0:
                    textoRetorno2 = ", indefinidamente";
                    break;

                case 1:
                    textoRetorno2 = ", até o dia " + Recorrencia_DataLimite.ToString("dd/MM/yyyy");
                    break;

                case 2:
                    textoRetorno2 = ", " + Recorrencia_N_Vezes + " vezes";
                    break;

                default:
                    textoRetorno2 = "???";
                    break;
            }


            return textoRetorno1 + textoRetorno2;
        }


        public static bool VerificarDiferencaInteiraDeMeses(DateTime data1, DateTime data2)
        {

            // Remove o horário das datas, mantendo apenas a data
            DateTime data1SemHorario = data1.Date;
            DateTime data2SemHorario = data2.Date;

            // Calcula a diferença entre os anos e os meses
            int anosDeDiferenca = data2SemHorario.Year - data1SemHorario.Year;
            int mesesDeDiferenca = data2SemHorario.Month + anosDeDiferenca * 12 - data1SemHorario.Month;

            // Verifica se a diferença é de um número inteiro de meses e se os dias são iguais
            return data1SemHorario.AddMonths(mesesDeDiferenca) == data2SemHorario && data1SemHorario.Day == data2SemHorario.Day;
        }

        public static bool VerificarDiferencaInteiraDeAnos(DateTime data1, DateTime data2)
        {
            // Remove o horário das datas, mantendo apenas a data
            DateTime data1SemHorario = data1.Date;
            DateTime data2SemHorario = data2.Date;

            // Calcula a diferença entre os anos
            int anosDeDiferenca = data2SemHorario.Year - data1SemHorario.Year;

            // Verifica se a diferença é de um número inteiro de anos e se os dias e meses são iguais
            return data1SemHorario.AddYears(anosDeDiferenca) == data2SemHorario &&
                   data1SemHorario.Day == data2SemHorario.Day &&
                   data1SemHorario.Month == data2SemHorario.Month;
        }

        public static int? CalcularMesesEntreDatas(DateTime dataInicial, DateTime dataFinal)
        {
            // Calcula a diferença total de meses entre as datas
            int meses = (dataFinal.Year - dataInicial.Year) * 12 + dataFinal.Month - dataInicial.Month;

            // Ajusta a contagem se o dia da data final for menor que o dia da data inicial
            if (dataFinal.Day < dataInicial.Day)
            {
                meses--;
            }

            return meses;
        }

        public static int? CalcularAnosEntreDatas(DateTime dataInicial, DateTime dataFinal)
        {
            // Calcula a diferença de anos
            int anos = dataFinal.Year - dataInicial.Year;

            // Ajusta a contagem se a data final ainda não passou o mês e dia da data inicial
            if (dataFinal.Month < dataInicial.Month ||
               dataFinal.Month == dataInicial.Month && dataFinal.Day < dataInicial.Day)
            {
                anos--;
            }

            return anos;
        }

        // RECEBE UMA DATA, DADOS DE RECORRÊNCIA, E RETORNA SE A DATA É COMPATÍVEL OU NÃO
        public static bool VerificaDataCompativel(DateTime DataHoje, DateTime DataTransacao, int Frequencia, DateTime DataLimite, int Modo, int N_Vezes)
        {
            // INDEFINIDAMENTE
            if (Modo == 1)
            {

                TimeSpan diferenca = DataTransacao.Date - DataHoje.Date;

                // VERIFICA SE A FREQUENCIA (DIARIA | SEMANAL | MENSAL | ANUAL) É COMPATÍVEL | SE A DataHoje É MULTIPLO DE DataTransacao
                switch (Frequencia)
                {

                    // SE FOR DIÁRIA
                    case 1: return diferenca.TotalDays >= 1 ? true : false;

                    // SE FOR SEMANAL
                    case 2: return diferenca.TotalDays % 7 == 0 ? true : false;

                    // SE FOR MENSAL
                    case 3: return VerificarDiferencaInteiraDeMeses(DataTransacao, DataHoje);

                    // SE FOR ANUAL
                    case 4: return VerificarDiferencaInteiraDeAnos(DataTransacao, DataHoje);

                    default: return false;

                }
            }

            // ATÉ O DIA
            if (Modo == 2)
            {
                // SE AINDA ESTÁ DENTRO DO LIMITE
                if (DataHoje <= DataLimite)
                {
                    TimeSpan diferenca = DataTransacao.Date - DataHoje.Date;

                    // VERIFICA SE A FREQUENCIA (DIARIA | SEMANAL | MENSAL | ANUAL) É COMPATÍVEL | SE A DataHoje É MULTIPLO DE DataTransacao
                    switch (Frequencia)
                    {

                        // SE FOR DIÁRIA
                        case 1: return diferenca.TotalDays >= 1 ? true : false;

                        // SE FOR SEMANAL
                        case 2: return diferenca.TotalDays % 7 == 0 ? true : false;

                        // SE FOR MENSAL
                        case 3: return VerificarDiferencaInteiraDeMeses(DataTransacao, DataHoje);

                        // SE FOR ANUAL
                        case 4: return VerificarDiferencaInteiraDeAnos(DataTransacao, DataHoje);

                        default: return false;

                    }

                }
                else
                {
                    return false;
                }
            }

            // NÚMERO DE VEZES
            if (Modo == 3)
            {
                TimeSpan diferenca = DataTransacao.Date - DataHoje.Date;

                switch (Frequencia)
                {

                    // SE FOR DIÁRIA
                    case 1: return diferenca.TotalDays <= N_Vezes ? true : false;

                    // SE FOR SEMANAL
                    case 2: return diferenca.TotalDays % 7 == 0 && diferenca.TotalDays / 7 <= N_Vezes ? true : false;

                    // SE FOR MENSAL
                    case 3: return VerificarDiferencaInteiraDeMeses(DataTransacao, DataHoje) && CalcularMesesEntreDatas(DataTransacao, DataHoje) <= N_Vezes ? true : false;

                    // SE FOR ANUAL
                    case 4: return VerificarDiferencaInteiraDeAnos(DataTransacao, DataHoje) && CalcularAnosEntreDatas(DataTransacao, DataHoje) <= N_Vezes ? true : false;

                    default: return false;

                }
            }

            return true;
        }






    }
}