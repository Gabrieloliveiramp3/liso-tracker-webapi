global using Microsoft.AspNetCore.Components.Authorization;
global using Blazored.LocalStorage;
using FinançasPessoaisWebAPI.FrontEnd;
using MudBlazor.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using FinançasPessoaisWebAPI.FrontEnd.Services;
using FinançasPessoaisWebAPI.FrontEnd.Helpers;
using ApexCharts;
using MudBlazor;
using Microsoft.Extensions.DependencyInjection;

namespace Finanças_Pessoais_Web_API_FrontEnd
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");
            builder.RootComponents.Add<HeadOutlet>("head::after");

            // CONFIGURAÇÃO DO SERVIÇO DE TOAST (MENSAGENS QUE APARECEM NO CANTO DA TELA)
            builder.Services.AddMudServices(config =>
            {
                config.SnackbarConfiguration.PositionClass = Defaults.Classes.Position.BottomCenter;
                config.SnackbarConfiguration.PreventDuplicates = false;
                config.SnackbarConfiguration.NewestOnTop = false;
                config.SnackbarConfiguration.ShowCloseIcon = true;
                config.SnackbarConfiguration.VisibleStateDuration = 10000;
                config.SnackbarConfiguration.HideTransitionDuration = 500;
                config.SnackbarConfiguration.ShowTransitionDuration = 500;
                config.SnackbarConfiguration.SnackbarVariant = Variant.Filled;
            });

            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            builder.Services.AddScoped<AuthenticationStateProvider, CustomAuthStateProvider>();
            builder.Services.AddAuthorizationCore();

            // SERVIÇO DE MUDBLAZOR
            builder.Services.AddMudServices();

            // SERVIÇO DE AUTENTICAÇÃO
            builder.Services.AddScoped<AuthenticationService>();

            // SERVIÇO DE TRANSAÇÃO
            builder.Services.AddScoped<TransactionService>();

            // SERVIÇO DE CONFIGURAÇÕES
            builder.Services.AddScoped<SettingsService>();

            builder.Services.AddBlazoredLocalStorage();
            
            await builder.Build().RunAsync();
        }
    }

}
