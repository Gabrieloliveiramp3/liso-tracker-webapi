﻿namespace FinançasPessoaisWebAPI.FrontEnd.Models
{
    public class User
    {
        // ATRIBUTOS
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
