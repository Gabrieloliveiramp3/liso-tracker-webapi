using MudBlazor;

namespace Finan�asPessoaisWebAPI.FrontEnd.Models
{
    public class TransacaoBD
    {
        // ID (n�o informado via API ainda!)
        public int Id { get; set; }

        // VALOR
        public decimal Valor { get; set; }

        // DESCRI��O
        public string Descricao { get; set; }

        // DATA
        public DateTime Data { get; set; }

        public string TextoChip { get; set; }


        // ID DA CONTA ('Conta Corrente' = 0, 'Poupan�a' = 1,'Carteira' = 2)
        public int ContaId { get; set; }

        // TIPO (INFORMADO PELO PR�PRIO FRONTEND J� QUE AS TRANSA��ES DE DESPESAS E RECEITAS S�O SEPARADAS NO ENDPOINT)
        public bool Tipo { get; set; }

        // CATEGORIA DA TRANSA��O (= categoriaReceita.categoriaReceita | categoriaDespesa.categoriaDespesa)
        public int Categoria { get; set; }



        // FREQU�NCIA DE RECORR�NCIA
        public int Recorrencia { get; set; }

        // MODO DE RECORR�NCIA
        public int Recorrencia_Modo { get; set; }

        // N�MERO DE VEZES QUE A RECORR�NCIA SE REPETE (�TIL A DEPENDER DO MODO ESCOLHIDO)
        public int Recorrencia_N_Vezes { get; set; }

        // DATA LIMITE DE REPETI��O DE RECORR�NCIA (�TIL A DEPENDER DO MODO ESCOLHIDO)
        public DateTime Recorrencia_Data_Limite { get; set; }

        // CONSTRUTOR
        public TransacaoBD(int id, decimal valor, string descricao, DateTime datatransa, string conta, bool tipo, string categoriaId, int recorrencia, int recorrencia_modo, int recorrencia_n_vezes, DateTime recorrencia_data_limite, string textoChip)
        {
            Id = id;
            Valor = valor;
            Descricao = descricao;
            Data = datatransa;

            if (conta == "ContaCorrente") ContaId = 0;
            if (conta == "Poupanca") ContaId = 1;
            if (conta == "Carteira") ContaId = 2;

            switch (categoriaId)
            {
                case "Sal�rio":
                    Categoria = 1;
                    break;

                case "Benef�cios":
                    Categoria = 2;
                    break;

                case "Vendas":
                    Categoria = 3;
                    break;

                case "Economias":
                    Categoria = 4;
                    break;

                case "Outros":
                    Categoria = 5;
                    break;

                case "Sa�de":
                    Categoria = 6;
                    break;

                case "Aluguel":
                    Categoria = 7;
                    break;

                case "Educa��o":
                    Categoria = 8;
                    break;

                case "Lazer":
                    Categoria = 9;
                    break;

                case "Alimenta��o":
                    Categoria = 10;
                    break;

                case "DespesasPessoais":
                    Categoria = 11;
                    break;

                default:
                    Categoria = 0;
                    break;
            }

            Tipo = tipo;

            Recorrencia = recorrencia;
            Recorrencia_Modo = recorrencia_modo;
            Recorrencia_N_Vezes = recorrencia_n_vezes;
            Recorrencia_Data_Limite = recorrencia_data_limite;

            TextoChip = textoChip;

        }

        // CONSTRUTOR
        public TransacaoBD(int id, decimal valor, string descricao, DateTime datatransa, int conta, bool tipo, int categoriaId, int recorrencia, int recorrencia_modo, int recorrencia_n_vezes, DateTime recorrencia_data_limite, string textoChip)
        {
            Id = id;
            Valor = valor;
            Descricao = descricao;
            Data = datatransa;
            Tipo = tipo;
            ContaId = conta;
            Categoria = categoriaId;
            Recorrencia = recorrencia;
            Recorrencia_Modo = recorrencia_modo;
            Recorrencia_N_Vezes = recorrencia_n_vezes;
            Recorrencia_Data_Limite = recorrencia_data_limite;
            TextoChip = textoChip;
        }

    }

}