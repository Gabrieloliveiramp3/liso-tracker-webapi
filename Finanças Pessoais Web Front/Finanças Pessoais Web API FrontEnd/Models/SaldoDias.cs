// FUNÇÕES AUXILIARES --------------------------------------------------------------
namespace FinançasPessoaisWebAPI.FrontEnd.Models
{
    public class SaldoDias
    {
        required public string Dia { get; set; }
        public decimal Saldo { get; set; }
    }
}