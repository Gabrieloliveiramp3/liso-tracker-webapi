public class TransacaoDTO
{
    // VALOR
    public decimal valor { get; set; }

    // DESCRI��O
    public string descricao { get; set; }

    // DATA
    public DateTime data { get; set; }

    // FREQU�NCIA DE RECORR�NCIA
    public int recorrencia { get; set; }

    // MODO DE RECORR�NCIA
    public int recorrenciaModo { get; set; }

    // N�MERO DE VEZES QUE A RECORR�NCIA SE REPETE (�TIL A DEPENDER DO MODO ESCOLHIDO)
    public int recorrenciaNVezes { get; set; }

    // DATA LIMITE DE REPETI��O DE RECORR�NCIA (�TIL A DEPENDER DO MODO ESCOLHIDO)
    public DateTime recorrenciaDataLimite { get; set; }

    public TransacaoDTO(decimal valorC, string descricaoC, DateTime dataC, int recorrenciaC, int recorrencia_modoC, int recorrencia_n_vezesC, DateTime recorrencia_data_limiteC)
    {
        valor = valorC;
        descricao = descricaoC;
        data = dataC;
        recorrencia = recorrenciaC;
        recorrenciaModo = recorrencia_modoC;
        recorrenciaNVezes = recorrencia_n_vezesC;
        recorrenciaDataLimite = recorrencia_data_limiteC;
    }
}