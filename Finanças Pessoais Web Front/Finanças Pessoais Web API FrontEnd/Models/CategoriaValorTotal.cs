    public class CategoriaValorTotal
    {
        required public int Categoria { get; set; }
        public decimal ValorTotal { get; set; }
    }