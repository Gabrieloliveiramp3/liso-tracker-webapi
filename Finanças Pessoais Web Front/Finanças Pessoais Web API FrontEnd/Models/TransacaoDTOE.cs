public class TransacaoDTOE
{
    // ID
    public int id { get; set; }

    // VALOR
    public decimal valor { get; set; }

    // DESCRI��O
    public string descricao { get; set; }

    // DATA
    public DateTime data { get; set; }

    // FREQU�NCIA DE RECORR�NCIA
    public int recorrencia { get; set; }

    // MODO DE RECORR�NCIA
    public int recorrenciaModo { get; set; }

    // N�MERO DE VEZES QUE A RECORR�NCIA SE REPETE (�TIL A DEPENDER DO MODO ESCOLHIDO)
    public int recorrenciaNVezes { get; set; }

    // DATA LIMITE DE REPETI��O DE RECORR�NCIA (�TIL A DEPENDER DO MODO ESCOLHIDO)
    public DateTime recorrenciaDataLimite { get; set; }
    public CategoriaPutDespesa? categoriaputDespesa { get; set; }
    public CategoriaPutReceita? categoriaputReceita { get; set; }

    public ContaDTO contaDto { get; set; }

    public TransacaoDTOE(bool Tipo, int idC,decimal valorC, string descricaoC, DateTime dataC, int recorrenciaC, int recorrencia_modoC, int recorrencia_n_vezesC, DateTime recorrencia_data_limiteC, string categoriaString, string conta)
    {
        id = idC;
        valor = valorC;
        descricao = descricaoC;
        data = dataC;
        recorrencia = recorrenciaC;
        recorrenciaModo = recorrencia_modoC;
        recorrenciaNVezes = recorrencia_n_vezesC;
        recorrenciaDataLimite = recorrencia_data_limiteC;

        contaDto = new ContaDTO();
        contaDto.tipoConta = conta;

        if (!Tipo)
        {
            categoriaputDespesa = new CategoriaPutDespesa();
            categoriaputDespesa.categoriaDespesa = categoriaString;
        }
        else
        {
            categoriaputReceita = new CategoriaPutReceita();
            categoriaputReceita.categoriaReceita = categoriaString;
        }

        
    }
}

public class CategoriaPutReceita
{
    public string? categoriaReceita { get; set; }
}

public class CategoriaPutDespesa
{
    public string? categoriaDespesa { get; set; }

}
