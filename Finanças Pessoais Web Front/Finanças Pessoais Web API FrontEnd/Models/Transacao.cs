namespace FinançasPessoaisWebAPI.FrontEnd.Models
{
    public class Transacao
    {   
        // ATRIBUTOS
        public int Id { get; set; }
        public decimal Valor { get; set;}
        public string Descricao { get; set;}
        public DateTime DataTransa { get; set;} 
        
        public int CategoriaId { get; set;}
        public int UsuarioId { get; set;}
        public int ContaId { get; set;}
        public bool Tipo { get; set;}
        public int Recorrencia { get; set;}
        public int Recorrencia_Modo { get; set;}
        public int Recorrencia_N_Vezes { get; set;}
        public DateTime Recorrencia_Data_Limite { get; set;}

        // CONSTRUTOR
        public Transacao(int id, int usuarioId, string descricao, decimal valor, int categoriaId, int contaId, DateTime datatransa, int recorrencia, int recorrencia_modo, int recorrencia_n_vezes, DateTime recorrencia_data_limite)
        {
            Id = id;
            UsuarioId = usuarioId;
            Descricao = descricao;
            Valor = valor;
            CategoriaId = categoriaId;
            DataTransa = datatransa;
            ContaId = contaId;
            Tipo = categoriaId == 1 ? true : false;

            Recorrencia = recorrencia;
            Recorrencia_Modo = recorrencia_modo;
            Recorrencia_N_Vezes = recorrencia_n_vezes;
            Recorrencia_Data_Limite = recorrencia_data_limite;
        }

    }

    

}