﻿using FinançasPessoaisWebAPI.FrontEnd.Pages.Before_Login_Pages;

namespace FinançasPessoaisWebAPI.FrontEnd.Models
{
    public class LoginModel
    {
        public string Email { get; set; } = "";
        public string Senha { get; set; } = "";
    

        public LoginModel(string emailC, string senhaC)
        {
            Email = emailC;
            Senha = senhaC;
        }
    }
}
