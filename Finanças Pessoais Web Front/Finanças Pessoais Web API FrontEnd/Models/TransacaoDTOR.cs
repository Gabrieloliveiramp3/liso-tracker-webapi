public class TransacaoDTOR
{
    public int id {  get; set; }
    public decimal valor { get; set; }
    public string descricao { get; set; }
    public DateTime data { get; set; }
    public int recorrencia { get; set; }
    public int recorrenciaModo { get; set; }
    public int recorrenciaNVezes { get; set; }
    public DateTime recorrenciaDataLimite { get; set; }

    public CategoriaReceitaDTO? categoriaReceita { get; set; }
    public CategoriaDespesaDTO? categoriaDespesa { get; set; }
    public ContaDTO conta { get; set; }
}

public class CategoriaReceitaDTO
{
    public string? tipoTransacao { get; set; }
    public string? categoriaReceita { get; set; }
}

public class CategoriaDespesaDTO
{
    public string? tipoTransacao { get; set; }
    public string? categoriaDespesa { get; set; }
}

public class ContaDTO
{
    public string tipoConta { get; set; }
}
