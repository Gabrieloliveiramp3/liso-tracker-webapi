// FUNÇÃO GAMBIARRA PARA CLICAR NUM BOTÃO VIRTUALMENTE
// POIS QUANDO CHAMO O MÉTODO DO BOTÃO VIA CÓDIGO O
// COMPORTAMENTO VISUAL NÃO É COMO ESPERADO

window.myFunctions = {
    clickButton: function() {
        setTimeout(function() {
            document.querySelector('.classeDoBotao').click();
        }, 1000);
    }
};