﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;
using Blazored.LocalStorage;
using FinançasPessoaisWebAPI.FrontEnd.Models;
using static System.Net.WebRequestMethods;
using FinançasPessoaisWebAPI.FrontEnd.Helpers;
using System.Text.Json;
using System.Text;
using System.Security.Cryptography;
using System.Text;
using Org.BouncyCastle.Crypto.Digests;
using System.Text;
using FinançasPessoaisWebAPI.FrontEnd.Services;

namespace FinançasPessoaisWebAPI.FrontEnd.Services
{
    public class AuthenticationService
    {
        private readonly HttpClient _httpClient;
        private readonly AuthenticationStateProvider _authenticationStateProvider;
        private readonly ILocalStorageService _localStorage;
        private readonly SettingsService _settingsService;

        public AuthenticationService(
            HttpClient httpClient,
            AuthenticationStateProvider authenticationStateProvider,
            ILocalStorageService localStorage,
            SettingsService settingsService)
        {
            _httpClient = httpClient;
            _authenticationStateProvider = authenticationStateProvider;
            _localStorage = localStorage;
            _settingsService = settingsService;
        }

        // MÉTODO PARA LOGAR USUÁRIO
        public async Task<bool> Login(LoginModel loginRequest)
        {
            // FAZ REQUISIÇÃO A API DE LOGIN
            var response = await _httpClient.PostAsJsonAsync("http://localhost:5299/Login", loginRequest);

            // VERIFICA O RESULTADO
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }
            // CRIA TODAS AS CONFIGURAÇÕES PADRÃO DO USUÁRIO
            await _settingsService.CriaConfiguracoes();

            var jsonResponse = await response.Content.ReadAsStringAsync();
            var jsonObject = JsonSerializer.Deserialize<Dictionary<string, object>>(jsonResponse);
            var token = jsonObject["token"].ToString();
            var admin = jsonObject["admin"].ToString();
            var nome = jsonObject["mensagem"].ToString();
            await _localStorage.SetItemAsync("token", token);
            await _localStorage.SetItemAsync("admin", admin);
            await _localStorage.SetItemAsync("nome", nome.Substring(10));
            await _localStorage.SetItemAsync("email", loginRequest.Email);

            

            ((CustomAuthStateProvider)_authenticationStateProvider).NotifyUserAuthentication(token);
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);

            return true;
        }

        // RETORNA O AVATAR DO USUÁRIO SALVO NO GRAVATAR
        public async Task<string> GetAvatarFromGravatar()
        {
            string email = await _localStorage.GetItemAsync<string>("email");
            var md5 = new MD5Digest();
            byte[] emailBytes = Encoding.ASCII.GetBytes(email.ToLower().Trim());
            md5.BlockUpdate(emailBytes, 0, emailBytes.Length);
            byte[] hash = new byte[md5.GetDigestSize()];
            md5.DoFinal(hash, 0);
            var sb = new StringBuilder(hash.Length * 2);
            foreach (byte b in hash)
            {
                sb.Append(b.ToString("x2"));
            }
            return "https://www.gravatar.com/avatar/" + sb.ToString();
        }

        // RETORNA O EMAIL DO USUÁRIO
        public async Task<string> GetUserEmail()
        {
            string email = await _localStorage.GetItemAsync<string>("email");
            return email;
        }

        // RETORNA O NOME DO USUÁRIO
        public async Task<string> GetUserName()
        {
            string email = await _localStorage.GetItemAsync<string>("nome");
            return email;
        }

        // MÉTODO PARA DESLOGAR USUÁRIO
        public async Task Logout()
        {
            await _localStorage.RemoveItemAsync("token");
            ((CustomAuthStateProvider)_authenticationStateProvider).NotifyUserLogout();
            _httpClient.DefaultRequestHeaders.Authorization = null;
        }


    }

}
