﻿using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;
using Blazored.LocalStorage;
using FinançasPessoaisWebAPI.FrontEnd.Models;
using System.Text.Json;
using System;
using ApexCharts;
using MudBlazor;

namespace FinançasPessoaisWebAPI.FrontEnd.Services
{
    public class TransactionService
    {
        private readonly HttpClient _httpClient;

        public TransactionService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        // MÉTODO PARA CRIAR TRANSAÇÃO
        public async Task<bool> CriarTransacao(bool Tipo, string Categoria, string ContaId, TransacaoDTO transacaoDTO)
        {
            var RC = Tipo ? "Receita/" : "Despesa/";

            var response = await _httpClient.PostAsJsonAsync("http://localhost:5299/Transacao/" + RC + Categoria + "/" + ContaId, transacaoDTO);

            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;
        }

        // MÉTODO PARA ALTERAR TRANSAÇÃO
        public async Task<bool> EditarTransacao(bool Tipo, TransacaoDTOE transacao)
        {
            var RC = Tipo ? "Receita" : "Despesa";

            // REQUISIÇÃO 
            var response = await _httpClient.PutAsJsonAsync("http://localhost:5299/" + RC, transacao);
            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;

        }

        // MÉTODO PARA DELETAR TRANSAÇÃO
        public async Task<bool> DeletarTransacao(int TransaID)
        {
            var response = await _httpClient.DeleteAsync($"http://localhost:5299/Transacao?id={TransaID}");

            if (!response.IsSuccessStatusCode)
            {
                return false;
            }

            return true;
        }

        // MÉTODO PARA RECEBER TRANSAÇÕES
        public async Task<List<TransacaoDTOR>> ReceberTransacoes(string URL)
        {
            var response = await _httpClient.GetAsync(URL);

            Console.WriteLine(response.ToString());

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                var listaTransacoes = JsonSerializer.Deserialize<List<TransacaoDTOR>>(jsonResponse);
                return listaTransacoes;
            }

            return null;
        }

        // MÉTODO PARA RECEBER TRANSAÇÃO PELO ID
        public async Task<TransacaoDTOR> ReceberTransacao(int TransaID)
        {
            var response = await _httpClient.GetAsync("http://localhost:5299/Transacao/" + TransaID);

            if (response.IsSuccessStatusCode)
            {
                var jsonResponse = await response.Content.ReadAsStringAsync();
                //Console.WriteLine(jsonResponse);
                var Transacao = JsonSerializer.Deserialize<TransacaoDTOR>(jsonResponse);
                return Transacao;
            }

            return null;
        }

    }
}
