﻿using System.Threading.Tasks;
using Blazored.LocalStorage;
using System.Linq.Dynamic.Core.Tokenizer;
using System.Globalization;

namespace FinançasPessoaisWebAPI.FrontEnd.Services
{
    public class SettingsService
    {
        private readonly ILocalStorageService _localStorage;

        public SettingsService(ILocalStorageService localStorage)
        {
            _localStorage = localStorage;
        }

        // MÉTODO PARA CRIAR TODAS AS CONFIGURAÇÕES COM VALOR PADRÃO NO STORAGE DO NAVEGADOR
        public async Task CriaConfiguracoes()
        {
            // FOTO DE PERFIL GRAVATAR (TRUE - RECEBE DO GRAVATAR | FALSE - INICIAL)
            await _localStorage.SetItemAsync("gravatarAvatar", true);

            // OCULTAR SALDO NO DASHBOARD (TRUE - OCULTA | FALSE - SEMPRE MOSTRA)
            await _localStorage.SetItemAsync("showValueConfiguration", true);

            // MOEDA UTILIZADA (POR PADRÃO RECEBE O DO SISTEMA, MAS PODE ALTERAR DEPOIS)
            await _localStorage.SetItemAsync("currencyConfiguration", CultureInfo.CurrentCulture.ToString());

            // TEMA DA APLICAÇÃO (0 - PEGAR DO SISTEMA | 1 - LIGHT THEME | 2 - DARK THEME)
            await _localStorage.SetItemAsync("darkThemeConfiguration", 0);
        }

        // MÉTODOS PARA RECUPERAR CADA CONFIGURAÇÃO ----------------------------------------
        
            // FOTO DE PERFIL GRAVATAR
            public async Task<bool> GetGravatarAvatarConfig()
            {
                // FOTO DE PERFIL GRAVATAR (TRUE - RECEBE DO GRAVATAR | FALSE - INICIAL)
                return await _localStorage.GetItemAsync<bool>("gravatarAvatar");
            }

            //  OCULTAR SALDO NO DASHBOARD
            public async Task<bool> GetShowValueConfig()
            {
                // FOTO DE PERFIL GRAVATAR (TRUE - RECEBE DO GRAVATAR | FALSE - INICIAL)
                return await _localStorage.GetItemAsync<bool>("showValueConfiguration");
            }

            // MOEDA UTILIZADA
            public async Task<string> GetCurrencyConfig()
            {
                // FOTO DE PERFIL GRAVATAR (TRUE - RECEBE DO GRAVATAR | FALSE - INICIAL)
                return await _localStorage.GetItemAsync<string>("currencyConfiguration");
            }

            // TEMA DA APLICAÇÃO
            public async Task<int> GetDarkThemeConfig()
            {
                // FOTO DE PERFIL GRAVATAR (TRUE - RECEBE DO GRAVATAR | FALSE - INICIAL)
                return await _localStorage.GetItemAsync<int>("darkThemeConfiguration");
            }


        // MÉDODOS PARA ALTERAR CADA CONFIGURAÇÃO ----------------------------------------
        
            // FOTO DE PERFIL GRAVATAR
            public async Task SetGravatarAvatarConfig(bool config)
            {
                await _localStorage.SetItemAsync("gravatarAvatar", config);
            }

            //  OCULTAR SALDO NO DASHBOARD
            public async Task SetShowValueConfig(bool config)
            {
                await _localStorage.SetItemAsync("showValueConfiguration", config);
            }

            // MOEDA UTILIZADA
            public async Task SetCurrencyConfig(string config)
            {
                await _localStorage.SetItemAsync("currencyConfiguration", config);
            }

            // TEMA DA APLICAÇÃO
            public async Task SetDarkThemeConfig(int config)
            {
                await _localStorage.SetItemAsync("darkThemeConfiguration", config);
            }


    }
}
