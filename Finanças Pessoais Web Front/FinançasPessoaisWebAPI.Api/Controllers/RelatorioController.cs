﻿using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using FinançasPessoaisWebAPI.Infra.Ioc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static FinançasPessoaisWebAPI.Infra.Data.Repositories.RelatorioRepository;

namespace FinançasPessoaisWebAPI.Api.Controllers;

[Route("[controller]")]
[ApiController]
[Authorize]
public class RelatorioController : Controller
{
    private readonly IRelatorioRepository _relatorioRepository;

    public RelatorioController(IRelatorioRepository relatorioRepository)
    {
        _relatorioRepository = relatorioRepository;
    }

    [HttpGet]
    public ResultadoRelatorio GetTransacaoReceitaeDespesas()
    {
        var userId = User.GetId();
        return _relatorioRepository.Relatorios(userId);
    }
}