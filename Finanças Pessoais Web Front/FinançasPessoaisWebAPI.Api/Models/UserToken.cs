﻿using Microsoft.AspNetCore.Mvc;

namespace FinançasPessoaisWebAPI.Api.Models;

public class UserToken
{
    public string Mensagem { get; set; }
    public string Token { get; set; }
    public object Admin { get; set; }

    public static explicit operator UserToken(ActionResult<UserToken> v)
    {
        throw new NotImplementedException();
    }
}