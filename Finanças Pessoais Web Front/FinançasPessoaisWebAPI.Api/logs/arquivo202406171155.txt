2024-06-17 11:55:02.241 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:02.253 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:02.254 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 204 null null 14.6626ms
2024-06-17 11:55:02.256 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:02.258 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:02.269 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:02.270 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:02.339 -03:00 [INF] Executed DbCommand (6ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:02.347 -03:00 [INF] Executed DbCommand (5ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:02.351 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:02.352 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 79.9394ms
2024-06-17 11:55:02.354 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:02.355 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 98.2754ms
2024-06-17 11:55:02.360 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:55:02.362 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:02.363 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 204 null null 2.7444ms
2024-06-17 11:55:02.365 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:55:02.367 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:02.368 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:02.369 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:02.373 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:02.381 -03:00 [INF] Executed DbCommand (4ms) [Parameters=[@__userId_0='1', @__p_1='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_1 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:02.383 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:02.385 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 14.3007ms
2024-06-17 11:55:02.386 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:02.388 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 200 null application/json; charset=utf-8 22.6861ms
2024-06-17 11:55:02.391 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:02.394 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:02.394 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 204 null null 3.0971ms
2024-06-17 11:55:02.397 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:02.399 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:02.400 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:02.401 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:02.407 -03:00 [INF] Executed DbCommand (3ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:02.413 -03:00 [INF] Executed DbCommand (3ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:02.415 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllDespesa, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:02.416 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 13.1094ms
2024-06-17 11:55:02.417 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:02.418 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 20.5563ms
2024-06-17 11:55:07.128 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:07.130 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:07.131 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:07.132 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:07.137 -03:00 [INF] Executed DbCommand (3ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:07.143 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:07.145 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:07.146 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 13.4443ms
2024-06-17 11:55:07.148 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:07.149 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 20.6912ms
2024-06-17 11:55:07.153 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:55:07.155 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:07.156 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:07.156 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:07.160 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:07.163 -03:00 [INF] Executed DbCommand (0ms) [Parameters=[@__userId_0='1', @__p_1='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_1 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:07.166 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:07.166 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 8.4288ms
2024-06-17 11:55:07.167 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:07.168 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 200 null application/json; charset=utf-8 15.2144ms
2024-06-17 11:55:07.171 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:07.173 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:07.174 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:07.175 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:07.178 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:07.181 -03:00 [INF] Executed DbCommand (0ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:07.184 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllDespesa, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:07.184 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 8.3597ms
2024-06-17 11:55:07.185 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:07.186 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 14.7314ms
2024-06-17 11:55:12.752 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:12.754 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:12.755 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 204 null null 2.7644ms
2024-06-17 11:55:12.757 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:12.759 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:12.760 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:12.761 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:12.764 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:12.767 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:12.770 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:12.771 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 9.0357ms
2024-06-17 11:55:12.772 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:12.772 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 15.0259ms
2024-06-17 11:55:12.967 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:55:12.970 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:12.971 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 204 null null 3.6149ms
2024-06-17 11:55:12.974 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:55:12.976 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:12.977 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:12.977 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:12.981 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:12.985 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_1 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:12.987 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:12.988 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 8.7744ms
2024-06-17 11:55:12.988 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:12.989 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 200 null application/json; charset=utf-8 15.4641ms
2024-06-17 11:55:12.995 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:12.997 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:12.997 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 204 null null 2.8423ms
2024-06-17 11:55:13.000 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:55:13.001 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:55:13.002 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:13.003 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:55:13.007 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:55:13.010 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:55:13.012 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllDespesa, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:55:13.014 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 9.4497ms
2024-06-17 11:55:13.015 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:55:13.016 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 16.6211ms
