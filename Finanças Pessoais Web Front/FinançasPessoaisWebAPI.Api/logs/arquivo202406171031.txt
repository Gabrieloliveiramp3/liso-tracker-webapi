2024-06-17 10:31:00.897 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/swagger/index.html - null null
2024-06-17 10:31:00.907 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/swagger/index.html - 200 null text/html;charset=utf-8 9.4278ms
2024-06-17 10:31:00.913 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/_framework/aspnetcore-browser-refresh.js - null null
2024-06-17 10:31:00.913 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/_vs/browserLink - null null
2024-06-17 10:31:00.916 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/_framework/aspnetcore-browser-refresh.js - 200 13790 application/javascript; charset=utf-8 2.703ms
2024-06-17 10:31:00.926 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/_vs/browserLink - 200 null text/javascript; charset=UTF-8 12.7316ms
2024-06-17 10:31:01.010 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/swagger/v1/swagger.json - null null
2024-06-17 10:31:01.025 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/swagger/v1/swagger.json - 200 null application/json;charset=utf-8 15.5422ms
2024-06-17 10:31:11.844 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 10:31:11.846 -03:00 [INF] CORS policy execution successful.
2024-06-17 10:31:11.846 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 204 null null 2.8604ms
2024-06-17 10:31:11.849 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 10:31:11.851 -03:00 [INF] CORS policy execution successful.
2024-06-17 10:31:11.852 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 10:31:11.856 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 10:31:11.910 -03:00 [INF] Executed DbCommand (5ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 10:31:11.917 -03:00 [INF] Executed DbCommand (5ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 10:31:11.920 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 10:31:11.923 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 64.8685ms
2024-06-17 10:31:11.924 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 10:31:11.924 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 75.711ms
2024-06-17 10:31:12.525 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 10:31:12.527 -03:00 [INF] CORS policy execution successful.
2024-06-17 10:31:12.528 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 204 null null 3.0392ms
2024-06-17 10:31:12.531 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 10:31:12.533 -03:00 [INF] CORS policy execution successful.
2024-06-17 10:31:12.533 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 10:31:12.534 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 10:31:12.538 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 10:31:12.545 -03:00 [INF] Executed DbCommand (4ms) [Parameters=[@__userId_0='1', @__p_1='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_1 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 10:31:12.547 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 10:31:12.548 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 13.1398ms
2024-06-17 10:31:12.550 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 10:31:12.551 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 200 null application/json; charset=utf-8 20.1297ms
2024-06-17 10:31:12.554 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 10:31:12.556 -03:00 [INF] CORS policy execution successful.
2024-06-17 10:31:12.557 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 204 null null 3.2491ms
2024-06-17 10:31:12.559 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 10:31:12.562 -03:00 [INF] CORS policy execution successful.
2024-06-17 10:31:12.562 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 10:31:12.563 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 10:31:12.569 -03:00 [INF] Executed DbCommand (3ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
2024-06-17 10:31:12.574 -03:00 [INF] Executed DbCommand (4ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 10:31:12.576 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllDespesa, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 10:31:12.578 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 13.3941ms
2024-06-17 10:31:12.579 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 10:31:12.580 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 20.5962ms
