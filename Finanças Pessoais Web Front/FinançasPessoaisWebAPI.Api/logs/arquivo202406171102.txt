2024-06-17 11:02:01.927 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:02:01.936 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:02:01.938 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 204 null null 32.4145ms
2024-06-17 11:02:01.940 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:02:01.942 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:02:01.943 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:02:01.944 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:02:01.949 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:02:01.952 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:02:01.954 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:02:01.955 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 9.7206ms
2024-06-17 11:02:01.956 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:02:01.957 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 16.5854ms
2024-06-17 11:02:02.093 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:02:02.095 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:02:02.096 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 204 null null 3.2515ms
2024-06-17 11:02:02.098 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - null null
2024-06-17 11:02:02.100 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:02:02.101 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:02:02.102 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:02:02.106 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:02:02.113 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Receita' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_1 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:02:02.116 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllReceita, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:02:02.117 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 14.345ms
2024-06-17 11:02:02.119 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:02:02.120 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Receita?NumerodaPagina=2&Quantidade=50 - 200 null application/json; charset=utf-8 21.7444ms
2024-06-17 11:02:02.125 -03:00 [INF] Request starting HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:02:02.127 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:02:02.128 -03:00 [INF] Request finished HTTP/1.1 OPTIONS http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 204 null null 2.8963ms
2024-06-17 11:02:02.130 -03:00 [INF] Request starting HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - null null
2024-06-17 11:02:02.131 -03:00 [INF] CORS policy execution successful.
2024-06-17 11:02:02.132 -03:00 [INF] Executing endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:02:02.133 -03:00 [INF] Route matched with {action = "GetAllTransacao", controller = "Transacao"}. Executing controller action with signature System.Threading.Tasks.Task`1[Microsoft.AspNetCore.Mvc.IActionResult] GetAllTransacaoAsync(TipoTransacao, FinançasPessoaisWebAPI.Api.Models.PaginasParams) on controller FinançasPessoaisWebAPI.Api.Controllers.TransacaoController (FinançasPessoaisWebAPI.Api).
2024-06-17 11:02:02.136 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1'], CommandType='"Text"', CommandTimeout='30']
SELECT COUNT(*)
FROM [Transacaos] AS [t]
LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
2024-06-17 11:02:02.140 -03:00 [INF] Executed DbCommand (1ms) [Parameters=[@__userId_0='1', @__p_1='0', @__p_2='50'], CommandType='"Text"', CommandTimeout='30']
SELECT [t0].[Id], [t0].[CategoriaId], [t0].[ContaId], [t0].[Data], [t0].[Descricao], [t0].[Recorrencia], [t0].[RecorrenciaDataLimite], [t0].[RecorrenciaModo], [t0].[RecorrenciaNVezes], [t0].[UsuarioId], [t0].[Valor], [t0].[Id0], [t0].[categoriaDespesa], [t0].[categoriaReceita], [t0].[tipoTransacao], [c0].[Id], [c0].[tipoConta], [u].[Id], [u].[Admin], [u].[Email], [u].[Nome], [u].[SenhaHash], [u].[SenhaNormal]
FROM (
    SELECT [t].[Id], [t].[CategoriaId], [t].[ContaId], [t].[Data], [t].[Descricao], [t].[Recorrencia], [t].[RecorrenciaDataLimite], [t].[RecorrenciaModo], [t].[RecorrenciaNVezes], [t].[UsuarioId], [t].[Valor], [c].[Id] AS [Id0], [c].[categoriaDespesa], [c].[categoriaReceita], [c].[tipoTransacao]
    FROM [Transacaos] AS [t]
    LEFT JOIN [Categorias] AS [c] ON [t].[CategoriaId] = [c].[Id]
    WHERE [c].[tipoTransacao] = N'Despesa' AND [t].[UsuarioId] = @__userId_0
    ORDER BY [t].[Data] DESC
    OFFSET @__p_1 ROWS FETCH NEXT @__p_2 ROWS ONLY
) AS [t0]
LEFT JOIN [Contas] AS [c0] ON [t0].[ContaId] = [c0].[Id]
INNER JOIN [Usuarios] AS [u] ON [t0].[UsuarioId] = [u].[Id]
ORDER BY [t0].[Data] DESC
2024-06-17 11:02:02.142 -03:00 [INF] Executing OkObjectResult, writing value of type 'FinançasPessoaisWebAPI.Domain.Paginacao.Paginas`1[[FinançasPessoaisWebAPI.Application.DTOs.Transacao.TransacaoGetAllDespesa, FinançasPessoaisWebAPI.Application, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]]'.
2024-06-17 11:02:02.143 -03:00 [INF] Executed action FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api) in 8.9496ms
2024-06-17 11:02:02.143 -03:00 [INF] Executed endpoint 'FinançasPessoaisWebAPI.Api.Controllers.TransacaoController.GetAllTransacaoAsync (FinançasPessoaisWebAPI.Api)'
2024-06-17 11:02:02.144 -03:00 [INF] Request finished HTTP/1.1 GET http://localhost:5299/Despesa?NumerodaPagina=1&Quantidade=50 - 200 null application/json; charset=utf-8 14.4081ms
