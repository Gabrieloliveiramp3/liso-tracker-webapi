﻿using FinançasPessoaisWebAPI.Api.Middleware;
using FinançasPessoaisWebAPI.Domain.Validators;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Ioc;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Serilog;
using System.Globalization;
using System.Reflection;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddValidatorsFromAssemblyContaining<UsuarioValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<CategoriaValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<TransacaoValidator>();
builder.Services.AddValidatorsFromAssemblyContaining<ContaValidator>();
builder.Services.AddFluentValidationAutoValidation();
builder.Services.AddControllers().AddFluentValidation(p =>
{
    p.ValidatorOptions.LanguageManager.Culture = new CultureInfo("pt-BR");
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddInfrastructureSwagger();
builder.Services.AddInfrastructure(builder.Configuration);

builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowMyOrigin",
    builder => builder.WithOrigins("http://localhost:7033", "https://localhost:7033")
    .AllowAnyMethod()
    .AllowAnyHeader());
});

builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Liso Tracker WebAPI",
        Version = "v1",
        Description = " \"Se você não comprar nada, o desconto é maior.\" ",
        Contact = new OpenApiContact
        {
            Name = "Gabriel Oliveira - Alessandro Beserra - Fabio Ricardo - Jose Victor - Wana Batista",

            Url = new Uri(
                "https://gitlab.com/Gabrieloliveiramp3/curso.net/-/tree/main/Finan%C3%A7as%20Pessoais%20Web%20API?ref_type=heads")
        }
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

builder.Services.AddControllers().AddJsonOptions(x =>
{
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());

});

builder.Services.AddDbContext<FinançasPessoaisWebApiApiContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")).EnableDetailedErrors());

builder.Services.AddSqlite<FinançasPessoaisWebApiApiContext>("Data Source=FinançasPessoais.db");

//LOGS
var logger = new LoggerConfiguration()
    .MinimumLevel.Information()
    .WriteTo.Console()
    .WriteTo.File("logs/arquivo.txt", rollingInterval: RollingInterval.Minute)
    .CreateLogger();

builder.Logging.ClearProviders();
builder.Logging.AddSerilog(logger);
//##############################

var app = builder.Build();
app.UseCors("AllowMyOrigin");
app.UseSwagger();
app.UseSwaggerUI(
   c =>
   {
       c.SwaggerEndpoint("/swagger/v1/swagger.json", "Liso Tracker WebAPI V.1");
       c.DefaultModelsExpandDepth(-1); // define a profundidade de expansão padrão para -1, o que efetivamente oculta todos os modelos (ou esquemas) na interface do Swagger UI.
   });

app.UseMiddleware<ExceptionMiddleware>();
app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();