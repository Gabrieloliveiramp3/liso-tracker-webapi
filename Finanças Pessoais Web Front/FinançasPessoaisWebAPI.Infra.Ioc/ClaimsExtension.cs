﻿using System.Security.Claims;

namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class ClaimsExtension
{
    public static int GetId(this ClaimsPrincipal user)
    {
        if (user.Identity.IsAuthenticated)
        {
            var claim = user.FindFirst("id");
            if (claim != null && int.TryParse(claim.Value, out int id))
            {
                return id;
            }
        }

        throw new InvalidOperationException("Usuario não encontrado ou não autenticado.");
    }

    public static string GetEmail(this ClaimsPrincipal user)
    {
        var claim = user.FindFirst("email");
        return claim?.Value ?? string.Empty;
    }
}