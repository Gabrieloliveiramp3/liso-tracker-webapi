﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class DependecyInjectionSwagger
{
    public static IServiceCollection AddInfrastructureSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            // Define uma definição de segurança para tokens Bearer
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Description = "Copie: `'Bearer' + Token`",
                Type = SecuritySchemeType.ApiKey,
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
            });

            // Especifica o requisito de segurança para tokens Bearer
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },                    
                    new string[] {}
                }
            });
        });

        services.AddSwaggerGen(options =>
        {
            //o Swagger está tentando gerar um schemaId para os tipos Categoria_Despesa+TipoTransacao e CategoriaReceitaDTO+TipoTransacao, mas ambos estão resultando no mesmo schemaId de “$TipoTransacao”.
            //Isso acontece porque ambos os tipos são enums com o mesmo nome (TipoTransacao) em classes diferentes (Categoria_Despesa e CategoriaReceitaDTO).
            options.CustomSchemaIds(type =>
            {
                if (type.IsEnum)
                {
                    return $"{type.DeclaringType.Name}{type.Name}"; //Esta função modificada gera IDs de esquema combinando o nome do tipo declarado e o nome da enumeração.
                }
                else
                {
                    return type.FullName;
                }
            });
        });

        return services;
    }
}