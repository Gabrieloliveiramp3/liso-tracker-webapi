﻿using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Application.Mappings;
using FinançasPessoaisWebAPI.Application.Services;
using FinançasPessoaisWebAPI.Domain.Account;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Data.Identity;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using FinançasPessoaisWebAPI.Infra.Data.Repositories;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace FinançasPessoaisWebAPI.Infra.Ioc;

public static class DependecyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddDbContext<FinançasPessoaisWebApiApiContext>(options =>
        {
            options.UseSqlServer(configuration.GetConnectionString("DefaultConnection"),
            b => b.MigrationsAssembly(typeof(FinançasPessoaisWebApiApiContext).Assembly.FullName));
        });

        services.AddAuthentication(opt =>
        {
            opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        }
        ).AddJwtBearer(options =>
        {
            // Configurar parâmetros de validação de token para autenticação JWT Bearer
            options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            {
                ValidateIssuer = true, // Valida o emissor do token
                ValidateAudience = true,
                ValidateLifetime = true,
                ValidateIssuerSigningKey = true,

                ValidIssuer = configuration["jwt:issuer"],
                ValidAudience = configuration["jwt:audience"],
                IssuerSigningKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["jwt:secretKey"])),
                ClockSkew = TimeSpan.Zero
            };
        });

        //Services

        services.AddScoped<ITransacaoService, TransacaoService>();
        services.AddScoped<IUsuarioService, UsuarioService>();
        services.AddScoped<IAuthenticate, AuthenticateService>();

        //Mapper
        services.AddAutoMapper(typeof(EntitiesDtoProfile));

        //Repository

        services.AddScoped<ITransacaoRepository, TransacaoRepository>();
        services.AddScoped<IUsuarioRepository, UsuarioRepository>();
        services.AddScoped<IRelatorioRepository, RelatorioRepository>();

        return services;
    }
}