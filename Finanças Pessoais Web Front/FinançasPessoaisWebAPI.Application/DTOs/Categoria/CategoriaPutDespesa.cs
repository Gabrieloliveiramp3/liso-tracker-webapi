﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Categoria
{
    public class CategoriaPutDespesa
    {
        public CategoriaDespesa categoriaDespesa { get; set; }
        public enum CategoriaDespesa
        {
            Outros,
            Saúde,
            Aluguel,
            Educação,
            Lazer,
            Alimentação,
            DespesasPessoais
        }
    }
}
