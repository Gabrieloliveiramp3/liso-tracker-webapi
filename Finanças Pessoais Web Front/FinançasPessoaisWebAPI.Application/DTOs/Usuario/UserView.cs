﻿using System.Text.Json.Serialization;

namespace FinançasPessoaisWebAPI.Application.DTOs.Usuario;

public class UserView
{
    public string Nome { get; set; }

    public string Email { get; set; }

    [JsonIgnore]
    public bool Admin { get; set; }
}