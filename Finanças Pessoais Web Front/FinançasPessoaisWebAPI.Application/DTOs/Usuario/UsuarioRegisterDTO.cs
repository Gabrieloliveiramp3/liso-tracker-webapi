﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace FinançasPessoaisWebAPI.Application.DTOs.Usuario;

public class UsuarioRegisterDto
{
    [Required(ErrorMessage = "0 nome é obrigatório")]
    [MaxLength(250, ErrorMessage = "O nome nao pode ter mais de 250 caracteres")]
    public string Nome { get; set; } = string.Empty;

    [Required(ErrorMessage = "0 E-mail é obrigatório")]
    [MaxLength(250, ErrorMessage = "0 E-mail nao pode ter mais de 200 caracteres")]
    [EmailAddress]
    public string Email { get; set; }

    [Required(ErrorMessage = "A senha é obrigatória.")]
    [MaxLength(100, ErrorMessage = "A senha deve ter, no maximo, 100 caracteres.")]
    [MinLength(10, ErrorMessage = "A senha deve ter, no minimo, 10 caracteres.")]
    [NotMapped]
    public string Senha { get; set; }

    [JsonIgnore]
    public bool Admin { get; set; }
}