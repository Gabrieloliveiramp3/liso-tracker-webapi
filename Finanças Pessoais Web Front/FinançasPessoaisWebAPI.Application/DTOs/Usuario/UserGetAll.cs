﻿namespace FinançasPessoaisWebAPI.Application.DTOs.Usuario;

public class UserGetAll
{
    public int Id { get; set; }
    public string Nome { get; set; }
    public string Email { get; set; }
    public bool Admin { get; set; }
}