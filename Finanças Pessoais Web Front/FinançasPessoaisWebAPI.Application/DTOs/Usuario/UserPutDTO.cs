using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FinançasPessoaisWebAPI.Application.DTOs.Usuario;

public class UserPutDto
{
    public int Id { get; set; }

    [MaxLength(100, ErrorMessage = "O campo nome deve ter, no maximo, 100 caracteres.")]
    [MinLength(1, ErrorMessage = "O campo nome deve ter, no minimo, 10 caracteres.")]
    public string Nome { get; set; } = string.Empty;

    [DataType(DataType.Password)]
    [MinLength(10, ErrorMessage = "A senha deve ter, no minimo, 10 caracteres.")]
    public string Senha { get; set; }

    public bool Admin { get; set; }
}