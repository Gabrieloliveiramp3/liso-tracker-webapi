﻿using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;
using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Application.DTOs.Transacao;

public class TransacaoDto
{
    [Required(ErrorMessage = "O valor é obrigatório.")]
    [Range(1, 999999, ErrorMessage = "Valor deve ser maior que zero")]
    public decimal Valor { get; set; }

    [MaxLength(50, ErrorMessage = "O campo Descrição pode até 50 caracteres")]
    public string Descricao { get; set; }

    [Range(typeof(DateTime), "2000-05-14T17:41:19.271Z", "3000-05-13T18:42:17.271Z",
     ErrorMessage = "Data no Formato incorreto")]
    public DateTime Data { get; set; } = new DateTime(2020, 7, 30, 12, 38, 16, DateTimeKind.Utc);
    public int Recorrencia { get; set; }
    public int RecorrenciaModo { get; set; }
    public int RecorrenciaNVezes { get; set; }
    public DateTime RecorrenciaDataLimite { get; set; } = DateTime.UtcNow.AddDays(10);
    public virtual CategoriaReceitaDto CategoriaReceita { get; set; }
    public virtual CategoriaDespesaDto CategoriaDespesa { get; set; }
    public virtual ContaDto Conta { get; set; }
}