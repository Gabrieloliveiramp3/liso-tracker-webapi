﻿using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FinançasPessoaisWebAPI.Application.DTOs.Transacao;

public class TransacaoPostDespesaDto
{
    [JsonIgnore]
    public int UsuarioId { get; set; }

    [JsonIgnore]
    public int Id { get; set; }

    [JsonIgnore]
    public int ContaId { get; set; }

    [JsonIgnore]
    public int CategoriaId { get; set; }

    [Required(ErrorMessage = "O valor é obrigatório.")]
    public decimal Valor { get; set; }

    [MaxLength(50, ErrorMessage = "O campo Descrição pode até 50 caracteres")]
    public string Descricao { get; set; }

    public DateTime Data { get; set; } = DateTime.Now.AddHours(-6);

    public int Recorrencia { get; set; }

    public int RecorrenciaModo { get; set; }

    public int RecorrenciaNVezes { get; set; }
    public DateTime? RecorrenciaDataLimite { get; set; } = DateTime.UtcNow.AddDays(10);

    [JsonIgnore]
    public virtual CategoriaDespesaDto CategoriaDespesa { get; set; }

    [JsonIgnore]
    public virtual ContaDto Conta { get; set; }
}