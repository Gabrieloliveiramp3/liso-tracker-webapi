﻿using FinançasPessoaisWebAPI.Application.DTOs.Categoria;
using FinançasPessoaisWebAPI.Application.DTOs.Conta;

namespace FinançasPessoaisWebAPI.Application.DTOs.Transacao;

public class TransacaoPutReceita
{
    public int Id { get; set; }
    public decimal Valor { get; set; }
    public string Descricao { get; set; }
    public DateTime Data { get; set; } = new DateTime(2020, 7, 30, 12, 38, 16, DateTimeKind.Utc);

    public int Recorrencia { get; set; }
    public int RecorrenciaModo { get; set; }
    public int RecorrenciaNVezes { get; set; }
    public DateTime? RecorrenciaDataLimite { get; set; } = DateTime.UtcNow.AddDays(10);
    public virtual CategoriaPutReceita CategoriaPutReceita { get; set; }

    public virtual ContaDto ContaDto { get; set; }

}





