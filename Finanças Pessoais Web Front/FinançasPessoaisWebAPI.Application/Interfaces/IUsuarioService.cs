using FinançasPessoaisWebAPI.Application.DTOs.Usuario;

namespace FinançasPessoaisWebAPI.Application.Interfaces;

public interface IUsuarioService
{
    Task<IEnumerable<UserGetAll>> GetAllUsuarioAsync();

    Task<UserGetAll> GetUsuarioByIdAsync(int id);

    Task<UsuarioDto> CreateUsuarioAsync(UsuarioRegisterDto usuarioRegisterDto);

    Task<UserPutDto> EditUsuarioAsync(UserPutDto usuarioDto);

    Task<UserView> RemoveUsuario(int id);

    Task<bool> UserExiste();
}