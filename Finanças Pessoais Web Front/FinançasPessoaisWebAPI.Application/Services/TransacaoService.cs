﻿using AutoMapper;
using FinançasPessoaisWebAPI.Application.DTOs.Transacao;
using FinançasPessoaisWebAPI.Application.Interfaces;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Paginacao;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;

namespace FinançasPessoaisWebAPI.Application.Services;

public class TransacaoService : ITransacaoService
{
    private readonly ITransacaoRepository _transacaoRepository;
    private readonly IMapper _mapper;
    private readonly IUsuarioRepository _usuarioRepository;
    private readonly FinançasPessoaisWebApiApiContext _context;

    public TransacaoService(ITransacaoRepository transacaoRepository, IMapper mapper, IUsuarioRepository usuarioRepository, FinançasPessoaisWebApiApiContext context)
    {
        _transacaoRepository = transacaoRepository;
        _mapper = mapper;
        _usuarioRepository = usuarioRepository;
        _context = context;
    }

    public async Task<TransacaoDto> GetTransacoesByUsuarioIdAsync(int userId, int id)
    {
        var usuario = await _transacaoRepository.GetTransacoesByUsuarioIdAsync(userId, id);
        return _mapper.Map<TransacaoDto>(usuario);
    }

    public async Task<TransacaoPostDespesaDto> CreateTransacaoDespesasAsync(int userId, TransacaoPostDespesaDto transacaoPostDespesaDto)
    {
        var transacao = _mapper.Map<Transacao>(transacaoPostDespesaDto);
        var transacaoCriada = await _transacaoRepository.CreateTransacaoDespesasAsync(userId, transacao);
        return _mapper.Map<TransacaoPostDespesaDto>(transacaoCriada);
    }

    public async Task<TransacaoPostReceitaDto> CreateTransacaoReceitasAsync(int userId, TransacaoPostReceitaDto transacaoReceita)
    {
        var transacao = _mapper.Map<Transacao>(transacaoReceita);
        var transacaoCriada = await _transacaoRepository.CreateTransacaoReceitasAsync(userId, transacao);
        return _mapper.Map<TransacaoPostReceitaDto>(transacaoCriada);
    }

    public async Task<TransacaoDto> RemoveTransacao(int userId, int id)
    {
        var transacaoExcluida = await _transacaoRepository.RemoveTransacao(userId, id);
        return _mapper.Map<TransacaoDto>(transacaoExcluida);
    }

    public async Task<Paginas<TransacaoGetAllReceita>> GetAllTransacaoReceitaAsync(int userId, int pageNumber, int pageSize)
    {
        var usuario = await _transacaoRepository.GetAllTransacaoReceitaAsync(userId, pageNumber, pageSize);
        var transacao = _mapper.Map<IEnumerable<TransacaoGetAllReceita>>(usuario);

        return new Paginas<TransacaoGetAllReceita>(transacao, pageNumber, pageSize, usuario.TotalCount);
    }

    public async Task<Paginas<TransacaoGetAllDespesa>> GetAllTransacaoDespesaAsync(int userId, int pageNumber, int pageSize)
    {
        var usuario = await _transacaoRepository.GetAllTransacaoDespesaAsync(userId, pageNumber, pageSize);
        var transacao = _mapper.Map<IEnumerable<TransacaoGetAllDespesa>>(usuario);

        return new Paginas<TransacaoGetAllDespesa>(transacao, pageNumber, pageSize, usuario.TotalCount);
    }


    public async Task<TransacaoPutReceita> EditTransacaoReceitaAsync(int userId, TransacaoPutReceita transacaoPutReceita)
    {
        var transacaoatual = _mapper.Map<Transacao>(transacaoPutReceita);
        var transacaoEditada = await _transacaoRepository.EditTransacaoReceitaAsync(userId,transacaoatual);
        return _mapper.Map<TransacaoPutReceita>(transacaoEditada);
    }

    public async Task<TransacaoPutDespesa> EditTransacaoDespesaAsync(int userId, TransacaoPutDespesa transacaoPutDespesa)
    {
        var transacao = _mapper.Map<Transacao>(transacaoPutDespesa);
        var transacaoEditada = await _transacaoRepository.EditTransacaoDespesaAsync(userId, transacao);
        return _mapper.Map<TransacaoPutDespesa>(transacaoEditada);
    }

}
