﻿namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Categoria
{
    public int Id { get; set; }

    public TipoTransacao tipoTransacao { get; set; }
    public CategoriaReceita? categoriaReceita { get; set; }
    public CategoriaDespesa? categoriaDespesa { get; set; }

    public enum TipoTransacao
    {
        Receita,
        Despesa
    }

    public ICollection<Transacao> Transacaos { get; set; } // Uma categoria pode ter várias transações

    public enum CategoriaReceita
    {
        Outros,
        Salário,
        Benefícios,
        Vendas,
        Economias
    }

    public enum CategoriaDespesa
    {
        Outros,
        Saúde,
        Aluguel,
        Educação,
        Lazer,
        Alimentação,
        DespesasPessoais
    }

    public Categoria()
    {
        //Hashset não permite duplicatas, com isso garantimos que não teremos Ids iguais quando for criado.
        Transacaos = new HashSet<Transacao>();
    }
}