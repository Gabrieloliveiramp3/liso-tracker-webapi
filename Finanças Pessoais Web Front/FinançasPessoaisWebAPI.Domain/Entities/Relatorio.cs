﻿namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Relatorio
{
    public decimal Soma;
    public Conta.TipoConta TipoConta;
    public string Categoria;

    public Categoria.TipoTransacao TipoTransacao { get; set; }
}