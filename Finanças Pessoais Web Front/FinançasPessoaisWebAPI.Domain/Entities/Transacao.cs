﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Transacao
{
    public int Id { get; set; }
    public int UsuarioId { get; set; }

    [Required(ErrorMessage = "O valor é obrigatório.")]
    [NotNull]
    public decimal Valor { get; set; }

    [MaxLength(50, ErrorMessage = "O campo Descrição pode até 50 caracteres")]
    public string Descricao { get; set; }

    public DateTime Data { get; set; } = new DateTime(2020, 7, 30, 12, 38, 16, DateTimeKind.Utc);
    public int Recorrencia { get; set; }
    public int? RecorrenciaModo { get; set; }
    public int? RecorrenciaNVezes { get; set; }
    public DateTime? RecorrenciaDataLimite { get; set; }

    // Propriedades de navegação
    public virtual Categoria Categoria { get; set; }

    public virtual Usuario Usuario { get; set; }
    public virtual Conta Conta { get; set; }
}

// Relacionamento Um para Muitos (1:N) : Uma transação deve ser registrada em apenas uma conta, mas uma conta pode ter diversas transações relacionadas.
//Relacionamento Um para Muitos(1:N): Uma transação deve ser registrada em apenas uma conta, mas uma conta pode ter diversas transações relacionadas.