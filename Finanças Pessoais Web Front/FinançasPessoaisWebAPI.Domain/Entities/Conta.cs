﻿namespace FinançasPessoaisWebAPI.Domain.Entities;

public class Conta
{
    public int Id { get; set; }

    public TipoConta tipoConta { get; set; }

    public enum TipoConta
    {
        Carteira,
        ContaCorrente,
        Poupanca
    }

    public ICollection<Transacao> Transacao { get; set; } //Uma conta pode ter várias transações

    public Conta()
    {
        //Hashset não permite duplicatas, com isso garantimos que não teremos Ids iguais quando for criado.
        Transacao = new HashSet<Transacao>();
    }
}