﻿using System.ComponentModel.DataAnnotations;

namespace FinançasPessoaisWebAPI.Domain.Paginacao;

public class Paginas<T> : List<T>
{
    public int CurrentPage { get; set; }
    public int TotalPages { get; set; }

    [Range(1, 50, ErrorMessage = "O máximo de itens por página são 50.")]
    public int Quantidade { get; set; }
    public int TotalCount { get; set; }

    public Paginas(IEnumerable<T> items, int pageNumber, int pageSize, int count)
    {
        CurrentPage = pageNumber;
        Console.WriteLine(pageNumber);
        Console.WriteLine(pageSize);
        Console.WriteLine(count);
        TotalPages = (int)Math.Ceiling(count / (decimal)pageSize);
        Quantidade = pageSize;
        TotalCount = count;

        AddRange(items);
    }

    public Paginas(IEnumerable<T> items, int currentPage, int totalPages, int pageSize, int totalCount)
    {
        CurrentPage = currentPage;
        TotalPages = totalPages;
        Quantidade = pageSize;
        TotalCount = totalCount;

        AddRange(items);
    }
}