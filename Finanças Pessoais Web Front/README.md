<img alt="🏦" aria-label="🏦" src="https://notion-emojis.s3-us-west-2.amazonaws.com/prod/svg-twitter/1f3e6.svg" width="250px">


<h1 >Título do Projeto: Finanças Pessoais Web API :money_with_wings:</h1>



<h4>O projeto "Finanças Pessoais Web API" tem como objetivo principal a criação de uma interface de programação de aplicativos (API) que permitirá aos usuários gerenciar suas finanças pessoais de maneira simples e eficiente. Através da API, os usuários poderão registrar suas despesas e receitas, categorizar transações, acompanhar saldos e gerar relatórios financeiros, além, disso poderão se conectar às suas contas bancárias através do open bank e ter as movimentações atualizadas automaticamente no sistema.</h4>

### Objetivos do Projeto:
- Aplicar os princípios da Programação Orientada a Objetos (POO) utilizando a linguagem C#. :green_check_mark:	
- Desenvolver uma API RESTful.
- Implementar operações de CRUD (Create, Read, Update, Delete).
- Garantir a segurança da API.
- Aplicar boas práticas de programação e design de API.


### Requisitos de Negócio:
>  Cadastro de despesas e  de capital, categorizacao, autenticacao de usuarios, relatorios 
### Estrutura do Projeto:
- ASP,NET Core para a criacao da  API RESTful
- Blazor para a interface do usuario.
- Entity Framework usando Sql server como BD
### Entregáveis:
- [ ] API Restiful
- [ ] FrontEnd consumindo a API
- [x] Choro e Lágrima :cry:	
### Metodologia:
…
### Recursos Necessários:
…
### Quais requisitos do cliente não puderam ser atendidos e por que?
…
# Avaliação:
#### O projeto será avaliado com base nos seguintes critérios:

1. Adesão aos princípios da POO e boas práticas de programação.
2. Funcionalidade e robustez da API desenvolvida.
3. Qualidade e clareza da documentação e do guia do desenvolvedor.
4. Estrutura e design do banco de dados.
5. Implementação e eficácia da autenticação e autorização.
6. Qualidade dos testes realizados (Opcional).

# Resultados Esperados:
> Os alunos deverão entregar uma Web API para controle financeiro pessoal totalmente funcional, acompanhada de documentação detalhada. A API deverá ser capaz de gerenciar usuários, transações financeiras, categorias, saldos e relatórios, tudo isso com um sistema de autenticação seguro. A experiência prática adquirida durante o projeto preparará os alunos para futuros desafios no campo do desenvolvimento de software.