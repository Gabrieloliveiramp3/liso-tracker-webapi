﻿using Bogus;
using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Context;

/* Aqui e feito o mapeamento que pegaram as entidades e
  irao para uma tabela no banco de dados pelo entityframework */

public class FinançasPessoaisWebApiApiContext : DbContext
{
  
    public FinançasPessoaisWebApiApiContext(DbContextOptions<FinançasPessoaisWebApiApiContext> options)
        : base(options)
    {
    }

    public DbSet<Usuario> Usuarios { get; set; }
    public DbSet<Transacao> Transacaos { get; set; }
    public DbSet<Categoria> Categorias { get; set; }
    public DbSet<Conta> Contas { get; set; }

    //Isso permite que veja informações detalhadas sobre os dados sensíveis durante a execução das consultas.
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.EnableSensitiveDataLogging();
        base.OnConfiguring(optionsBuilder);
    }


    //Na Pasta EntitiesConfiguration, está mapeado como cada tabela deve se comportar

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.ApplyConfiguration(new CategoriaConfiguration());
        modelBuilder.ApplyConfiguration(new ContaConfiguration());
        modelBuilder.ApplyConfiguration(new TransacaoConfiguration());
        modelBuilder.ApplyConfiguration(new UsuarioConfiguration());


        

        var userFaker = new Faker<Usuario>(locale: "pt_BR")
         .RuleFor(u => u.Id, 1)
         .RuleFor(u => u.Nome, f => f.Person.FirstName)
         .RuleFor(u => u.Email, f => f.Internet.Email());

        modelBuilder.Entity<Usuario>().HasData(userFaker);


        var startDate = new DateTime(2024, 01, 01, 9, 0, 0);
        var endDate = new DateTime(2024, 05, 28, 17, 0, 0);
        var categoriaId = 1;
        var contaId = 1;
        var transacaoId = 1;
        var number = 1000;

        var categoriaDespesafake = new Faker<Categoria>()
         .RuleFor(u => u.Id, f => categoriaId++)
         .RuleFor(u => u.tipoTransacao, Categoria.TipoTransacao.Despesa)
         .RuleFor(u => u.categoriaDespesa, f => f.PickRandom<Categoria.CategoriaDespesa>())
         .Generate(number / 2);
        modelBuilder.Entity<Categoria>().HasData(categoriaDespesafake);

        var categoriaReceitafake = new Faker<Categoria>()
         .RuleFor(u => u.Id, f => categoriaId++)
         .RuleFor(u => u.tipoTransacao, Categoria.TipoTransacao.Receita)
         .RuleFor(u => u.categoriaReceita, f => f.PickRandom<Categoria.CategoriaReceita>())
         .Generate(number / 2);
        modelBuilder.Entity<Categoria>().HasData(categoriaReceitafake);

        var contafake = new Faker<Conta>()
         .RuleFor(u => u.Id, f => contaId++)
         .RuleFor(u => u.tipoConta, f => f.PickRandom<Conta.TipoConta>())
         .Generate(number);
        modelBuilder.Entity<Conta>().HasData(contafake);

        var faker = new Faker<Transacao>(locale: "pt_BR")

            .RuleFor(c => c.Id, f => transacaoId++)
            .RuleFor(t => t.Valor, f => f.Finance.Amount())
            .RuleFor(t => t.Descricao, f => f.Lorem.Word())
            .RuleFor(t => t.Data, f => f.Date.Between(startDate, endDate))
            .RuleFor(t => t.UsuarioId, 2)
            .Generate(number);
        modelBuilder.Entity<Transacao>().HasData(faker);
    }
}