﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using static FinançasPessoaisWebAPI.Domain.Entities.Categoria;

namespace FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration;

public class CategoriaConfiguration : IEntityTypeConfiguration<Categoria>
{
    public void Configure(EntityTypeBuilder<Categoria> builder)
    {
        builder.HasKey(c => c.Id);

        builder.Property(p => p.tipoTransacao).HasConversion<string>()
            .HasConversion(v => v.ToString(),
                           v => (TipoTransacao)Enum.Parse(typeof(TipoTransacao), v));
        builder.Property(p => p.categoriaReceita).HasConversion<string>()
            .HasConversion(v => v.ToString(),
                           v => (CategoriaReceita)Enum.Parse(typeof(CategoriaReceita), v));
        builder.Property(p => p.categoriaDespesa).HasConversion<string>()
            .HasConversion(v => v.ToString(),
                           v => (CategoriaDespesa)Enum.Parse(typeof(CategoriaDespesa), v));

        builder.Property(c => c.tipoTransacao)
            .IsRequired();

        builder.Property(c => c.categoriaReceita);

        builder.Property(c => c.categoriaDespesa);
    }
}