﻿using FinançasPessoaisWebAPI.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using static FinançasPessoaisWebAPI.Domain.Entities.Conta;

namespace FinançasPessoaisWebAPI.Infra.Data.EntitiesConfiguration;

public class ContaConfiguration : IEntityTypeConfiguration<Conta>
{
    public void Configure(EntityTypeBuilder<Conta> builder)
    {
        builder.HasKey(c => c.Id);

        builder.Property(p => p.tipoConta)
           .IsRequired()
           .HasConversion<string>()
           .HasConversion(v => v.ToString(),
                          v => (TipoConta)Enum.Parse(typeof(TipoConta), v));

        builder.HasMany(c => c.Transacao)
            .WithOne(t => t.Conta);
    }
}