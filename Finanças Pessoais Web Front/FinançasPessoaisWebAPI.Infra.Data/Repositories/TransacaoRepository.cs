﻿using FinançasPessoaisWebAPI.Domain.Entities;
using FinançasPessoaisWebAPI.Domain.Paginacao;
using FinançasPessoaisWebAPI.Infra.Data.Context;
using FinançasPessoaisWebAPI.Infra.Data.Helper;
using FinançasPessoaisWebAPI.Infra.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using static FinançasPessoaisWebAPI.Domain.Entities.Categoria;

namespace FinançasPessoaisWebAPI.Infra.Data.Repositories;

public class TransacaoRepository : ITransacaoRepository
{
    private readonly FinançasPessoaisWebApiApiContext _transacao;
    private readonly FinançasPessoaisWebApiApiContext _categoria;

    public TransacaoRepository(FinançasPessoaisWebApiApiContext transacao, FinançasPessoaisWebApiApiContext categoria)
    {
        _transacao = transacao;
        _categoria = categoria;
    }

    public async Task<Transacao> CreateTransacaoReceitasAsync(int userId, Transacao transacao)
    {
        if (transacao == null)
        {
            throw new ArgumentNullException(nameof(transacao), "O objeto de transação fornecido não pode ser nulo.");
        }

        try
        {
            var existingTransacao = await _transacao.Transacaos
                .Where(c => c.UsuarioId == userId && c.Id == transacao.Id)
                .FirstOrDefaultAsync();

            if (existingTransacao != null)
            {
                throw new Exception($"Já existe uma transação com o mesmo ID ({transacao.Id}) para o usuário {userId}.");
            }

            await _transacao.Transacaos.AddAsync(transacao);
            await _transacao.SaveChangesAsync();

            return transacao;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao salvar a transação de despesa", ex);
        }
        catch (Exception ex)
        {
            throw new Exception("Ocorreu um erro ao salvar a transação", ex);
        }
    }

    public async Task<Transacao> CreateTransacaoDespesasAsync(int userId, Transacao transacao)
    {
        if (transacao == null)
        {
            throw new ArgumentNullException(nameof(transacao), "O objeto de transação fornecido não pode ser nulo.");
        }

        try
        {
            var existingTransacao = await _transacao.Transacaos
                .Where(c => c.UsuarioId == userId && c.Id == transacao.Id)
                .FirstOrDefaultAsync();

            if (existingTransacao != null)
            {
                throw new Exception($"Já existe uma transação com o mesmo ID ({transacao.Id}) para o usuário {userId}.");
            }

            await _transacao.Transacaos.AddAsync(transacao);
            await _transacao.SaveChangesAsync();

            return transacao;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao salvar a transação de despesa", ex);
        }
        catch (Exception ex)
        {
            throw new Exception("Ocorreu um erro ao salvar a transação", ex);
        }
    }

    public async Task<Transacao> GetTransacoesByUsuarioIdAsync(int userId, int id)
    {
        try
        {
            return await _transacao.Transacaos
                .Include(p => p.Categoria)
                .Include(t => t.Conta)
                .Include(u => u.Usuario)
                .Where(c => c.UsuarioId == userId && c.Id == id)
                .AsNoTracking()
                .SingleOrDefaultAsync();
        }
        catch (Exception ex)
        {
            // Log the exception and handle it appropriately
            throw new Exception($"An error occurred while retrieving transaction with ID {id} for user ID {userId}.", ex);
        }
    }

    public async Task<Paginas<Transacao>> GetAllTransacaoReceitaAsync(int userId, int pageNumber, int pageSize)
    {
        try
        {
            var query = _transacao.Transacaos
                .Include(c => c.Categoria).Where(c => c.Categoria.tipoTransacao == Categoria.TipoTransacao.Receita)
                .Include(t => t.Conta)
                .Include(u => u.Usuario).Where(c => c.UsuarioId == userId)
                .OrderByDescending(x => x.Data)
                .AsNoTracking().AsQueryable();
            return await PaginasHelper.CreateAsync(query, pageNumber, pageSize);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao recuperar transações para o ID do usuário {userId}.", ex);
        }
    }

    public async Task<Paginas<Transacao>> GetAllTransacaoDespesaAsync(int userId, int pageNumber, int pageSize)
    {
        try
        {
            var query = _transacao.Transacaos
                .Include(c => c.Categoria).Where(c => c.Categoria.tipoTransacao == Categoria.TipoTransacao.Despesa)
                .Include(t => t.Conta)
                .Include(u => u.Usuario).Where(c => c.UsuarioId == userId)
                .OrderByDescending(x => x.Data)
                .AsNoTracking().AsQueryable();
            return await PaginasHelper.CreateAsync(query, pageNumber, pageSize);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao recuperar todas as transações de despesas do ID do usuário {userId}.", ex);
        }
    }

    public async Task<Transacao> EditTransacaoReceitaAsync(int userId, Transacao transacao)
    {
        if (transacao == null)
        {
            throw new ArgumentNullException(nameof(transacao), "O objeto de transação não pode ser nulo.");
        }

        try
        {
            var transacaoAtual = await _transacao.Transacaos
            .Include(c => c.Conta)
            .Include(c => c.Categoria)
            .Where(c => c.UsuarioId == userId && c.Id == transacao.Id)
            .FirstOrDefaultAsync();

            if (transacaoAtual == null)
            {
                return null;
            }

            transacaoAtual.Data = transacao.Data;
            transacaoAtual.Descricao = transacao.Descricao;
            transacaoAtual.Valor = transacao.Valor;

            transacaoAtual.Recorrencia = transacao.Recorrencia;
            transacaoAtual.RecorrenciaModo = transacao.RecorrenciaModo;
            transacaoAtual.RecorrenciaNVezes = transacao.RecorrenciaNVezes;
            transacaoAtual.RecorrenciaDataLimite = transacao.RecorrenciaDataLimite;

            transacaoAtual.Categoria.categoriaReceita = transacao.Categoria.categoriaReceita;
            transacaoAtual.Conta.tipoConta = transacao.Conta.tipoConta;
            _transacao.Transacaos.Update(transacaoAtual);
            await _transacao.SaveChangesAsync();

            return transacaoAtual;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao atualizar a transação com ID {transacao.Id}.", ex);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao atualizar a transação com ID {transacao.Id}.", ex);
        }
    }


    public async Task<Transacao> EditTransacaoDespesaAsync(int userId, Transacao transacao)
    {
        if (transacao == null)
        {
            throw new ArgumentNullException(nameof(transacao), "O objeto de transação não pode ser nulo.");
        }

        try
        {
            var transacaoAtual = await _transacao.Transacaos
            .Include(c => c.Conta)
            .Include(c => c.Categoria)
            .Where(c => c.UsuarioId == userId && c.Id == transacao.Id)
            .FirstOrDefaultAsync();

            if (transacaoAtual == null)
            {
                return null;
            }

            transacaoAtual.Data = transacao.Data;
            transacaoAtual.Descricao = transacao.Descricao;
            transacaoAtual.Valor = transacao.Valor;

            transacaoAtual.Recorrencia = transacao.Recorrencia;
            transacaoAtual.RecorrenciaModo = transacao.RecorrenciaModo;
            transacaoAtual.RecorrenciaNVezes = transacao.RecorrenciaNVezes;
            transacaoAtual.RecorrenciaDataLimite = transacao.RecorrenciaDataLimite;

            transacaoAtual.Categoria.categoriaDespesa = transacao.Categoria.categoriaDespesa;
            transacaoAtual.Conta.tipoConta = transacao.Conta.tipoConta;

            _transacao.Transacaos.Update(transacaoAtual);
            await _transacao.SaveChangesAsync();

            return transacaoAtual;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao atualizar a transação com ID {transacao.Id}.", ex);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao atualizar a transação com ID {transacao.Id}.", ex);
        }
    }

    public async Task<Transacao> RemoveTransacao(int userId, int id)
    {
        try
        {
            var transacao = await _transacao.Transacaos
                .Where(c => c.UsuarioId == userId && c.Id == id)
                .FirstOrDefaultAsync();
            if (transacao != null)
            {
                _transacao.Transacaos.Remove(transacao);
                await _transacao.SaveChangesAsync();
                return transacao;
            }
            return null;
        }
        catch (DbUpdateException ex)
        {
            throw new Exception($"Ocorreu um erro ao remover a transação com ID {id}.", ex);
        }
        catch (Exception ex)
        {
            throw new Exception($"Ocorreu um erro ao remover a transação com ID {id}.", ex);
        }
    }
}