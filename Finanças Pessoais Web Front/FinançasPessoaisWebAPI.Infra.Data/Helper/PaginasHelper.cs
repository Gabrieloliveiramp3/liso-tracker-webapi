﻿using FinançasPessoaisWebAPI.Domain.Paginacao;
using Microsoft.EntityFrameworkCore;

namespace FinançasPessoaisWebAPI.Infra.Data.Helper;

public class PaginasHelper
{
    public static async Task<Paginas<T>> CreateAsync<T>
            (IQueryable<T> source, int pageNumber, int pageSize) where T : class
    {
        var count = await source.CountAsync();
        var items = await source.Skip((pageNumber - 1) * pageSize).Take((pageSize)).ToListAsync();
        return new Paginas<T>(items, pageNumber, pageSize, count);
    }
}