﻿using FinançasPessoaisWebAPI.Infra.Data.Repositories;

namespace FinançasPessoaisWebAPI.Infra.Data.Interfaces;

public interface IRelatorioRepository
{
    RelatorioRepository.ResultadoRelatorio Relatorios(int userId);
}